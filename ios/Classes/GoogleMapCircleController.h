// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import <Flutter/Flutter.h>
#import <GoogleMaps/GoogleMaps.h>

// Defines circle UI options writable from Flutter.
@protocol FLTGoogleMapCircleOptionsSink
- (void)setClickable:(BOOL)clickable;
- (void)setFillColor:(UIColor*)fillColor;
- (void)setPosition:(CLLocationCoordinate2D)position;
- (void)setRadius:(CLLocationDistance)radius;
- (void)setStrokeColor:(UIColor*)strokeColor;
- (void)setStrokeWidth:(float)strokeWidth;
- (void)setVisible:(BOOL)visible;
- (void)setZIndex:(int)zIndex;
@end

// Defines circle controllable by Flutter.
@interface FLTGoogleMapCircleController : NSObject <FLTGoogleMapCircleOptionsSink>
@property(atomic, readonly) NSString* circleId;
- (instancetype)initWithPosition:(CLLocationCoordinate2D)position mapView:(GMSMapView*)mapView;
@end
