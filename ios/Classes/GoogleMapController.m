// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "GoogleMapController.h"

#pragma mark - Conversion of JSON-like values sent via platform channels. Forward declarations.

static id positionToJson(GMSCameraPosition* position);
static double toDouble(id json);
static UIColor* colorFromInt(int colorInt);
static CLLocationCoordinate2D toLocation(id json);
static GMSPath* toPath(id json);
static NSArray<CLLocation*>* toLocationArray(id json);
static NSArray<GMUWeightedLatLng*>* toWeightedLocationArray(id json);
static GMSCameraPosition* toOptionalCameraPosition(id json);
static GMSCoordinateBounds* toOptionalBounds(id json);
static GMSCameraUpdate* toCameraUpdate(id json);
static void interpretMapOptions(id json, id<FLTGoogleMapOptionsSink> sink);
static void interpretCircleOptions(id json, id<FLTGoogleMapCircleOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar);
static void interpretHeatMapOptions(id json, id<FLTGoogleMapHeatMapOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar);
static void interpretMarkerOptions(id json, id<FLTGoogleMapMarkerOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar);
static void interpretPolylineOptions(id json, id<FLTGoogleMapPolylineOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar);
static void interpretPolygonOptions(id json, id<FLTGoogleMapPolygonOptionsSink> sink,
                                     NSObject<FlutterPluginRegistrar>* registrar);

@implementation FLTGoogleMapFactory {
    NSObject<FlutterPluginRegistrar>* _registrar;
}

- (instancetype)initWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    self = [super init];
    if (self) {
        _registrar = registrar;
    }
    return self;
}

- (NSObject<FlutterMessageCodec>*)createArgsCodec {
    return [FlutterStandardMessageCodec sharedInstance];
}

- (NSObject<FlutterPlatformView>*)createWithFrame:(CGRect)frame
                                   viewIdentifier:(int64_t)viewId
                                        arguments:(id _Nullable)args {
    return [[FLTGoogleMapController alloc] initWithFrame:frame
                                          viewIdentifier:viewId
                                               arguments:args
                                               registrar:_registrar];
}
@end

@implementation FLTGoogleMapController {
    GMSMapView* _mapView;
    int64_t _viewId;
    NSMutableDictionary* _circles;
    NSMutableDictionary* _heatMaps;
    NSMutableDictionary* _markers;
    NSMutableDictionary* _polylines;
    NSMutableDictionary* _polygons;
    FlutterMethodChannel* _channel;
    BOOL _trackCameraPosition;
    NSObject<FlutterPluginRegistrar>* _registrar;
}

- (instancetype)initWithFrame:(CGRect)frame
               viewIdentifier:(int64_t)viewId
                    arguments:(id _Nullable)args
                    registrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    if ([super init]) {
        _viewId = viewId;
        
        GMSCameraPosition* camera = toOptionalCameraPosition(args[@"cameraPosition"]);
        _mapView = [GMSMapView mapWithFrame:frame camera:camera];
        _circles = [NSMutableDictionary dictionaryWithCapacity:1];
        _heatMaps = [NSMutableDictionary dictionaryWithCapacity:1];
        _markers = [NSMutableDictionary dictionaryWithCapacity:1];
        _polylines = [NSMutableDictionary dictionaryWithCapacity:1];
        _polygons = [NSMutableDictionary dictionaryWithCapacity:1];
        _trackCameraPosition = NO;
        interpretMapOptions(args, self);
        NSString* channelName =
        [NSString stringWithFormat:@"plugins.flutter.io/google_maps_%lld", viewId];
        _channel = [FlutterMethodChannel methodChannelWithName:channelName
                                               binaryMessenger:registrar.messenger];
        __weak __typeof__(self) weakSelf = self;
        [_channel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
            if (weakSelf) {
                [weakSelf onMethodCall:call result:result];
            }
        }];
        _mapView.delegate = weakSelf;
    }
    return self;
}

- (UIView*)view {
    return _mapView;
}

- (void)onMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([call.method isEqualToString:@"map#show"]) {
        [self showAtX:toDouble(call.arguments[@"x"]) Y:toDouble(call.arguments[@"y"])];
        result(nil);
    } else if ([call.method isEqualToString:@"map#hide"]) {
        [self hide];
        result(nil);
    } else if ([call.method isEqualToString:@"camera#animate"]) {
        [self animateWithCameraUpdate:toCameraUpdate(call.arguments[@"cameraUpdate"])];
        result(nil);
    } else if ([call.method isEqualToString:@"camera#move"]) {
        [self moveWithCameraUpdate:toCameraUpdate(call.arguments[@"cameraUpdate"])];
        result(nil);
    } else if ([call.method isEqualToString:@"map#update"]) {
        interpretMapOptions(call.arguments[@"options"], self);
        result(positionToJson([self cameraPosition]));
    } else if ([call.method isEqualToString:@"map#waitForMap"]) {
        result(nil);
    } else if ([call.method isEqualToString:@"circle#add"]) {
        NSDictionary* options = call.arguments[@"options"];
        NSString* circleId = [self addCircleWithPosition:toLocation(options[@"center"])];
        interpretCircleOptions(options, [self circleWithId:circleId], _registrar);
        result(circleId);
    } else if ([call.method isEqualToString:@"circle#update"]) {
        interpretCircleOptions(call.arguments[@"options"],
                               [self circleWithId:call.arguments[@"circle"]], _registrar);
        result(nil);
    } else if ([call.method isEqualToString:@"circle#remove"]) {
        [self removeCircleWithId:call.arguments[@"circle"]];
        result(nil);
    }else if ([call.method isEqualToString:@"heatMap#add"]) {
        NSDictionary* options = call.arguments[@"options"];
        id data = options[@"weightedData"];
        NSString* heatMapId = nil;
        if(data){
            heatMapId = [self addHeatMapWithWeightedData:toWeightedLocationArray(data)];
        }else{
            heatMapId = [self addHeatMapWithData:toLocationArray(data)];
        }
        interpretHeatMapOptions(options, [self heatMapWithId:heatMapId], _registrar);
        result(heatMapId);
    } else if ([call.method isEqualToString:@"heatMap#update"]) {
        FLTGoogleMapHeatMapController* heatMap = [self heatMapWithId:call.arguments[@"heatMap"]];
        if(heatMap){
            interpretHeatMapOptions(call.arguments[@"options"],heatMap, _registrar);
        }
        result(nil);
    } else if ([call.method isEqualToString:@"heatMap#remove"]) {
        [self removeHeatMapWithId:call.arguments[@"heatMap"]];
        result(nil);
    }else if ([call.method isEqualToString:@"marker#add"]) {
        NSDictionary* options = call.arguments[@"options"];
        NSString* markerId = [self addMarkerWithPosition:toLocation(options[@"position"])];
        interpretMarkerOptions(options, [self markerWithId:markerId], _registrar);
        result(markerId);
    } else if ([call.method isEqualToString:@"marker#update"]) {
        interpretMarkerOptions(call.arguments[@"options"],
                               [self markerWithId:call.arguments[@"marker"]], _registrar);
        result(nil);
    } else if ([call.method isEqualToString:@"marker#remove"]) {
        [self removeMarkerWithId:call.arguments[@"marker"]];
        result(nil);
    } else if ([call.method isEqualToString:@"polyline#add"]) {
        NSDictionary* options = call.arguments[@"options"];
        NSString* polylineId = [self addPolylineWithPath:toPath(options[@"points"])];
        interpretPolylineOptions(options, [self polylineWithId:polylineId], _registrar);
        result(polylineId);
    } else if ([call.method isEqualToString:@"polyline#update"]) {
        interpretPolylineOptions(call.arguments[@"options"],
                                 [self polylineWithId:call.arguments[@"polyline"]], _registrar);
        result(nil);
    } else if ([call.method isEqualToString:@"polyline#remove"]) {
        [self removePolylineWithId:call.arguments[@"polyline"]];
        result(nil);
    } else if ([call.method isEqualToString:@"polygon#add"]) {
        NSDictionary* options = call.arguments[@"options"];
        NSString* polygonId = [self addPolygonWithPath:toPath(options[@"points"])];
        interpretPolygonOptions(options, [self polygonWithId:polygonId], _registrar);
        result(polygonId);
    } else if ([call.method isEqualToString:@"polygon#update"]) {
        interpretPolygonOptions(call.arguments[@"options"],
                                 [self polygonWithId:call.arguments[@"polygon"]], _registrar);
        result(nil);
    } else if ([call.method isEqualToString:@"polygon#remove"]) {
        [self removePolygonWithId:call.arguments[@"polygon"]];
        result(nil);
    } else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)showAtX:(CGFloat)x Y:(CGFloat)y {
    _mapView.frame =
    CGRectMake(x, y, CGRectGetWidth(_mapView.frame), CGRectGetHeight(_mapView.frame));
    _mapView.hidden = NO;
}

- (void)hide {
    _mapView.hidden = YES;
}

- (void)animateWithCameraUpdate:(GMSCameraUpdate*)cameraUpdate {
    [_mapView animateWithCameraUpdate:cameraUpdate];
}

- (void)moveWithCameraUpdate:(GMSCameraUpdate*)cameraUpdate {
    [_mapView moveCamera:cameraUpdate];
}

- (GMSCameraPosition*)cameraPosition {
    if (_trackCameraPosition) {
        return _mapView.camera;
    } else {
        return nil;
    }
}
- (NSString*)addCircleWithPosition:(CLLocationCoordinate2D)position {
    FLTGoogleMapCircleController* circleController =
    [[FLTGoogleMapCircleController alloc] initWithPosition:position mapView:_mapView];
    _circles[circleController.circleId] = circleController;
    return circleController.circleId;
}

- (FLTGoogleMapCircleController*)circleWithId:(NSString*)circleId {
    return _circles[circleId];
}

- (void)removeCircleWithId:(NSString*)circleId {
    FLTGoogleMapCircleController* circleController = _circles[circleId];
    if (circleController) {
        [circleController setVisible:NO];
        [_circles removeObjectForKey:circleId];
    }
}
- (NSString*)addHeatMapWithData:(NSArray<CLLocation *> *)data {
    FLTGoogleMapHeatMapController* heatMapController =
    [[FLTGoogleMapHeatMapController alloc] initWithData:data mapView:_mapView];
    _heatMaps[heatMapController.heatMapId] = heatMapController;
    return heatMapController.heatMapId;
}
- (NSString*)addHeatMapWithWeightedData:(NSArray<GMUWeightedLatLng *> *)weightedData{
    FLTGoogleMapHeatMapController* heatMapController =
    [[FLTGoogleMapHeatMapController alloc] initWithWeightedData:weightedData mapView:_mapView];
    _heatMaps[heatMapController.heatMapId] = heatMapController;
    return heatMapController.heatMapId;
}
- (FLTGoogleMapHeatMapController*)heatMapWithId:(NSString*)heatMapId {
    return _heatMaps[heatMapId];
}

- (void)removeHeatMapWithId:(NSString*)heatMapId {
    FLTGoogleMapHeatMapController* heatMapController = _heatMaps[heatMapId];
    if (heatMapController) {
        [heatMapController setVisible:NO];
        [_heatMaps removeObjectForKey:heatMapId];
    }
}
- (NSString*)addMarkerWithPosition:(CLLocationCoordinate2D)position {
    FLTGoogleMapMarkerController* markerController =
    [[FLTGoogleMapMarkerController alloc] initWithPosition:position mapView:_mapView];
    _markers[markerController.markerId] = markerController;
    return markerController.markerId;
}

- (FLTGoogleMapMarkerController*)markerWithId:(NSString*)markerId {
    return _markers[markerId];
}

- (void)removeMarkerWithId:(NSString*)markerId {
    FLTGoogleMapMarkerController* markerController = _markers[markerId];
    if (markerController) {
        [markerController setVisible:NO];
        [_markers removeObjectForKey:markerId];
    }
}

- (NSString*)addPolylineWithPath:(GMSPath*)path {
    FLTGoogleMapPolylineController* polylineController =
    [[FLTGoogleMapPolylineController alloc] initWithPath:path mapView:_mapView];
    _polylines[polylineController.polylineId] = polylineController;
    return polylineController.polylineId;
}

- (FLTGoogleMapPolylineController*)polylineWithId:(NSString*)polylineId {
    return _polylines[polylineId];
}

- (void)removePolylineWithId:(NSString*)polylineId {
    FLTGoogleMapPolylineController* polylineController = _polylines[polylineId];
    if (polylineController) {
        [polylineController setVisible:NO];
        [_polylines removeObjectForKey:polylineId];
    }
}

- (NSString*)addPolygonWithPath:(GMSPath*)path {
    FLTGoogleMapPolygonController* polygonController =
    [[FLTGoogleMapPolygonController alloc] initWithPath:path mapView:_mapView];
    _polygons[polygonController.polygonId] = polygonController;
    return polygonController.polygonId;
}

- (FLTGoogleMapPolygonController*)polygonWithId:(NSString*)polygonId {
    return _polygons[polygonId];
}

- (void)removePolygonWithId:(NSString*)polygonId {
    FLTGoogleMapPolygonController* polygonController = _polygons[polygonId];
    if (polygonController) {
        [polygonController setVisible:NO];
        [_polygons removeObjectForKey:polygonId];
    }
}

#pragma mark - FLTGoogleMapOptionsSink methods

- (void)setCamera:(GMSCameraPosition*)camera {
    _mapView.camera = camera;
}

- (void)setCameraTargetBounds:(GMSCoordinateBounds*)bounds {
    _mapView.cameraTargetBounds = bounds;
}

- (void)setCompassEnabled:(BOOL)enabled {
    _mapView.settings.compassButton = enabled;
}

- (void)setMapType:(GMSMapViewType)mapType {
    _mapView.mapType = mapType;
}

- (void)setMinZoom:(float)minZoom maxZoom:(float)maxZoom {
    [_mapView setMinZoom:minZoom maxZoom:maxZoom];
}

- (void)setRotateGesturesEnabled:(BOOL)enabled {
    _mapView.settings.rotateGestures = enabled;
}

- (void)setScrollGesturesEnabled:(BOOL)enabled {
    _mapView.settings.scrollGestures = enabled;
}

- (void)setTiltGesturesEnabled:(BOOL)enabled {
    _mapView.settings.tiltGestures = enabled;
}

- (void)setTrackCameraPosition:(BOOL)enabled {
    _trackCameraPosition = enabled;
}

- (void)setZoomGesturesEnabled:(BOOL)enabled {
    _mapView.settings.zoomGestures = enabled;
}

- (void)setMyLocationEnabled:(BOOL)enabled {
    _mapView.myLocationEnabled = enabled;
    _mapView.settings.myLocationButton = enabled;
}

#pragma mark - GMSMapViewDelegate methods

- (void)mapView:(GMSMapView*)mapView willMove:(BOOL)gesture {
    [_channel invokeMethod:@"camera#onMoveStarted" arguments:@{@"isGesture" : @(gesture)}];
}

- (void)mapView:(GMSMapView*)mapView didChangeCameraPosition:(GMSCameraPosition*)position {
    if (_trackCameraPosition) {
        [_channel invokeMethod:@"camera#onMove" arguments:@{@"position" : positionToJson(position)}];
    }
}

- (void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition*)position {
    [_channel invokeMethod:@"camera#onIdle" arguments:@{}];
}

- (void)mapView:(GMSMapView*)mapView didTapOverlay:(nonnull GMSOverlay *)overlay {
    if([overlay class] == [GMSCircle class]){
        NSString* circleId = overlay.userData[0];
        [_channel invokeMethod:@"circle#onTap" arguments:@{@"circle" : circleId}];
    }else if([overlay class] == [GMSPolyline class]){
        NSString* polylineId = overlay.userData[0];
        [_channel invokeMethod:@"polyline#onTap" arguments:@{@"polyline" : polylineId}];
    }else if([overlay class] == [GMSPolygon class]){
        NSString* polygonId = overlay.userData[0];
        [_channel invokeMethod:@"polygon#onTap" arguments:@{@"polygon" : polygonId}];
    }
}

- (BOOL)mapView:(GMSMapView*)mapView didTapMarker:(GMSMarker*)marker {
    NSString* markerId = marker.userData[0];
    [_channel invokeMethod:@"marker#onTap" arguments:@{@"marker" : markerId}];
    return [marker.userData[1] boolValue];
}

- (void)mapView:(GMSMapView*)mapView didTapInfoWindowOfMarker:(GMSMarker*)marker {
    NSString* markerId = marker.userData[0];
    [_channel invokeMethod:@"infoWindow#onTap" arguments:@{@"marker" : markerId}];
}

@end

#pragma mark - Implementations of JSON conversion functions.
static UIColor* colorFromInt(int colorInt){
    CGFloat red = (CGFloat) ((colorInt>>16) & 0xFF);
    CGFloat green = (CGFloat) ((colorInt>>8) & 0xFF);
    CGFloat blue = (CGFloat) ((colorInt) &0xFF);
    return [UIColor colorWithRed:(red/255) green:(green/255) blue:(blue/255) alpha:1.0];
}

static id locationToJson(CLLocationCoordinate2D position) {
    return @[ @(position.latitude), @(position.longitude) ];
}

static id positionToJson(GMSCameraPosition* position) {
    if (!position) {
        return nil;
    }
    return @{
             @"target" : locationToJson([position target]),
             @"zoom" : @([position zoom]),
             @"bearing" : @([position bearing]),
             @"tilt" : @([position viewingAngle]),
             };
}

static bool toBool(id json) {
    NSNumber* data = json;
    return data.boolValue;
}

static int toInt(id json) {
    NSNumber* data = json;
    return data.intValue;
}

static double toDouble(id json) {
    NSNumber* data = json;
    return data.doubleValue;
}

static float toFloat(id json) {
    NSNumber* data = json;
    return data.floatValue;
}

static CLLocationCoordinate2D toLocation(id json) {
    NSArray* data = json;
    return CLLocationCoordinate2DMake(toDouble(data[0]), toDouble(data[1]));
}

static GMSPath* toPath(id json){
    NSArray* data = json;
    GMSMutablePath* path = [GMSMutablePath path];
    for(id object in data){
        NSArray* d = object;
        [path addCoordinate:CLLocationCoordinate2DMake(toDouble(d[0]), toDouble(d[1]))];
    }
    return path;
}

static NSArray<GMSPath*>* toHolesArray(id json){
    NSArray* data = json;
    NSMutableArray<GMSPath*>* holes = [NSMutableArray new];
    for(id object in data){
        GMSPath* hole = toPath(object);
        [holes addObject:hole];
    }
    return holes;
}

static NSArray<CLLocation*>* toLocationArray(id json){
    NSArray* data = json;
    NSMutableArray<CLLocation*>* locations = [NSMutableArray new];
    for(id object in data){
        NSArray* d = object;
        CLLocation* location = [[CLLocation alloc] initWithLatitude:toDouble(d[0]) longitude:toDouble(d[1])];
        [locations addObject:location];
    }
    return locations;
}

static NSArray<GMUWeightedLatLng*>* toWeightedLocationArray(id json){
    NSArray* data = json;
    NSMutableArray<GMUWeightedLatLng*>* locations = [NSMutableArray new];
    for(id object in data){
        CLLocationCoordinate2D point = toLocation(object[@"point"]);
        double intensity = toDouble(object[@"intensity"]);
        [locations addObject:[[GMUWeightedLatLng alloc] initWithCoordinate:point intensity:intensity]];
    }
    return locations;
}

static GMUGradient* toGradient(id json){
    NSDictionary* data = json;
    NSMutableArray<UIColor*>* colors = [NSMutableArray new];
    NSArray* colorsJson = data[@"colors"];
    for(id color in colorsJson){
        [colors addObject:colorFromInt(toInt(color))];
    }
    NSMutableArray<NSNumber*>* stops = [NSMutableArray new];
    NSArray* stopsJson = data[@"stops"];
    for(id stop in stopsJson){
        [stops addObject:[NSNumber numberWithDouble:toDouble(stop)]];
    }
    return [[GMUGradient alloc] initWithColors:colors startPoints:stops colorMapSize:sizeof(colors)];
}

static CGPoint toPoint(id json) {
    NSArray* data = json;
    return CGPointMake(toDouble(data[0]), toDouble(data[1]));
}

static GMSCameraPosition* toCameraPosition(id json) {
    NSDictionary* data = json;
    return [GMSCameraPosition cameraWithTarget:toLocation(data[@"target"])
                                          zoom:toFloat(data[@"zoom"])
                                       bearing:toDouble(data[@"bearing"])
                                  viewingAngle:toDouble(data[@"tilt"])];
}

static GMSCameraPosition* toOptionalCameraPosition(id json) {
    return json ? toCameraPosition(json) : nil;
}

static GMSCoordinateBounds* toBounds(id json) {
    NSArray* data = json;
    return [[GMSCoordinateBounds alloc] initWithCoordinate:toLocation(data[0])
                                                coordinate:toLocation(data[1])];
}

static GMSCoordinateBounds* toOptionalBounds(id json) {
    NSArray* data = json;
    return (data[0] == [NSNull null]) ? nil : toBounds(data[0]);
}

static GMSMapViewType toMapViewType(id json) {
    int value = toInt(json);
    return (GMSMapViewType)(value == 0 ? 5 : value);
}

static GMSCameraUpdate* toCameraUpdate(id json) {
    NSArray* data = json;
    NSString* update = data[0];
    if ([update isEqualToString:@"newCameraPosition"]) {
        return [GMSCameraUpdate setCamera:toCameraPosition(data[1])];
    } else if ([update isEqualToString:@"newLatLng"]) {
        return [GMSCameraUpdate setTarget:toLocation(data[1])];
    } else if ([update isEqualToString:@"newLatLngBounds"]) {
        return [GMSCameraUpdate fitBounds:toBounds(data[1]) withPadding:toDouble(data[2])];
    } else if ([update isEqualToString:@"newLatLngZoom"]) {
        return [GMSCameraUpdate setTarget:toLocation(data[1]) zoom:toFloat(data[2])];
    } else if ([update isEqualToString:@"scrollBy"]) {
        return [GMSCameraUpdate scrollByX:toDouble(data[1]) Y:toDouble(data[2])];
    } else if ([update isEqualToString:@"zoomBy"]) {
        if (data.count == 2) {
            return [GMSCameraUpdate zoomBy:toFloat(data[1])];
        } else {
            return [GMSCameraUpdate zoomBy:toFloat(data[1]) atPoint:toPoint(data[2])];
        }
    } else if ([update isEqualToString:@"zoomIn"]) {
        return [GMSCameraUpdate zoomIn];
    } else if ([update isEqualToString:@"zoomOut"]) {
        return [GMSCameraUpdate zoomOut];
    } else if ([update isEqualToString:@"zoomTo"]) {
        return [GMSCameraUpdate zoomTo:toFloat(data[1])];
    }
    return nil;
}

static void interpretMapOptions(id json, id<FLTGoogleMapOptionsSink> sink) {
    NSDictionary* data = json;
    id cameraPosition = data[@"cameraPosition"];
    if (cameraPosition) {
        [sink setCamera:toCameraPosition(cameraPosition)];
    }
    id cameraTargetBounds = data[@"cameraTargetBounds"];
    if (cameraTargetBounds) {
        [sink setCameraTargetBounds:toOptionalBounds(cameraTargetBounds)];
    }
    id compassEnabled = data[@"compassEnabled"];
    if (compassEnabled) {
        [sink setCompassEnabled:toBool(compassEnabled)];
    }
    id mapType = data[@"mapType"];
    if (mapType) {
        [sink setMapType:toMapViewType(mapType)];
    }
    id minMaxZoomPreference = data[@"minMaxZoomPreference"];
    if (minMaxZoomPreference) {
        NSArray* zoomData = minMaxZoomPreference;
        float minZoom = (zoomData[0] == [NSNull null]) ? kGMSMinZoomLevel : toFloat(zoomData[0]);
        float maxZoom = (zoomData[1] == [NSNull null]) ? kGMSMaxZoomLevel : toFloat(zoomData[1]);
        [sink setMinZoom:minZoom maxZoom:maxZoom];
    }
    id rotateGesturesEnabled = data[@"rotateGesturesEnabled"];
    if (rotateGesturesEnabled) {
        [sink setRotateGesturesEnabled:toBool(rotateGesturesEnabled)];
    }
    id scrollGesturesEnabled = data[@"scrollGesturesEnabled"];
    if (scrollGesturesEnabled) {
        [sink setScrollGesturesEnabled:toBool(scrollGesturesEnabled)];
    }
    id tiltGesturesEnabled = data[@"tiltGesturesEnabled"];
    if (tiltGesturesEnabled) {
        [sink setTiltGesturesEnabled:toBool(tiltGesturesEnabled)];
    }
    id trackCameraPosition = data[@"trackCameraPosition"];
    if (trackCameraPosition) {
        [sink setTrackCameraPosition:toBool(trackCameraPosition)];
    }
    id zoomGesturesEnabled = data[@"zoomGesturesEnabled"];
    if (zoomGesturesEnabled) {
        [sink setZoomGesturesEnabled:toBool(zoomGesturesEnabled)];
    }
    id myLocationEnabled = data[@"myLocationEnabled"];
    if (myLocationEnabled) {
        [sink setMyLocationEnabled:toBool(myLocationEnabled)];
    }
}

static void interpretCircleOptions(id json, id<FLTGoogleMapCircleOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar) {
    NSDictionary* data = json;
    id center = data[@"center"];
    if (center) {
        [sink setPosition:toLocation(center)];
    }
    id radius = data[@"radius"];
    if (radius) {
        [sink setRadius:(CLLocationDistance)toDouble(radius)];
    }
    id fillColor = data[@"fillColor"];
    if (fillColor) {
        [sink setFillColor:colorFromInt(toInt(fillColor))];
    }
    id strokeColor = data[@"strokeColor"];
    if (strokeColor) {
        [sink setStrokeColor:colorFromInt(toInt(strokeColor))];
    }
    id strokeWidth = data[@"strokeWidth"];
    if (strokeWidth) {
        [sink setStrokeWidth:(CGFloat)toFloat(strokeWidth)];
    }
    id clickable = data[@"clickable"];
    if (clickable) {
        [sink setClickable:toBool(clickable)];
    }
    id visible = data[@"visible"];
    if (visible) {
        [sink setVisible:toBool(visible)];
    }
    id zIndex = data[@"zIndex"];
    if (zIndex) {
        [sink setZIndex:toInt(zIndex)];
    }
}

static void interpretHeatMapOptions(id json, id<FLTGoogleMapHeatMapOptionsSink> sink,
                                    NSObject<FlutterPluginRegistrar>* registrar) {
    NSDictionary* data = json;
    id radius = data[@"radius"];
    if (radius) {
        [sink setRadius:(CLLocationDistance)toDouble(radius)];
    }
    id opacity = data[@"opacity"];
    if (opacity) {
        [sink setOpacity:toFloat(opacity)];
    }
    id visible = data[@"visible"];
    if (visible) {
        [sink setVisible:toBool(visible)];
    }
    id points = data[@"data"];
    if (points) {
        [sink setData:toLocationArray(points)];
    }
    id weightedData = data[@"weightedData"];
    if (weightedData) {
        [sink setWeightedData:toWeightedLocationArray(weightedData)];
    }
    id gradient = data[@"gradient"];
    if (gradient) {
        [sink setGradient:toGradient(gradient)];
    }
}

static void interpretMarkerOptions(id json, id<FLTGoogleMapMarkerOptionsSink> sink,
                                   NSObject<FlutterPluginRegistrar>* registrar) {
    NSDictionary* data = json;
    id alpha = data[@"alpha"];
    if (alpha) {
        [sink setAlpha:toFloat(alpha)];
    }
    id anchor = data[@"anchor"];
    if (anchor) {
        [sink setAnchor:toPoint(anchor)];
    }
    id draggable = data[@"draggable"];
    if (draggable) {
        [sink setDraggable:toBool(draggable)];
    }
    id icon = data[@"icon"];
    if (icon) {
        NSArray* iconData = icon;
        UIImage* image;
        if ([iconData[0] isEqualToString:@"defaultMarker"]) {
            CGFloat hue = (iconData.count == 1) ? 0.0f : toDouble(iconData[1]);
            image = [GMSMarker markerImageWithColor:[UIColor colorWithHue:hue / 360.0
                                                               saturation:1.0
                                                               brightness:0.7
                                                                    alpha:1.0]];
        } else if ([iconData[0] isEqualToString:@"fromAsset"]) {
            if (iconData.count == 2) {
                image = [UIImage imageNamed:[registrar lookupKeyForAsset:iconData[1]]];
            } else {
                image = [UIImage imageNamed:[registrar lookupKeyForAsset:iconData[1]
                                                             fromPackage:iconData[2]]];
            }
        }
        [sink setIcon:image];
    }
    id flat = data[@"flat"];
    if (flat) {
        [sink setFlat:toBool(flat)];
    }
    id infoWindowAnchor = data[@"infoWindowAnchor"];
    if (infoWindowAnchor) {
        [sink setInfoWindowAnchor:toPoint(infoWindowAnchor)];
    }
    id infoWindowText = data[@"infoWindowText"];
    if (infoWindowText) {
        NSArray* infoWindowTextData = infoWindowText;
        NSString* title = (infoWindowTextData[0] == [NSNull null]) ? nil : infoWindowTextData[0];
        NSString* snippet = (infoWindowTextData[1] == [NSNull null]) ? nil : infoWindowTextData[1];
        [sink setInfoWindowTitle:title snippet:snippet];
    }
    id position = data[@"position"];
    if (position) {
        [sink setPosition:toLocation(position)];
    }
    id rotation = data[@"rotation"];
    if (rotation) {
        [sink setRotation:toDouble(rotation)];
    }
    id visible = data[@"visible"];
    if (visible) {
        [sink setVisible:toBool(visible)];
    }
    id zIndex = data[@"zIndex"];
    if (zIndex) {
        [sink setZIndex:toInt(zIndex)];
    }
}

static void interpretPolylineOptions(id json, id<FLTGoogleMapPolylineOptionsSink> sink,
                                     NSObject<FlutterPluginRegistrar>* registrar) {
    NSDictionary* data = json;
    
    id points = data[@"points"];
    if (points) {
        [sink setPoints:toPath(points)];
    }
    id clickable = data[@"consumeTapEvents"];
    if (clickable) {
        [sink setClickable:toBool(clickable)];
    }
    id color = data[@"color"];
    if (color) {
        [sink setColor:colorFromInt(toInt(color))];
    }
    id geodesic = data[@"geodesic"];
    if (geodesic) {
        [sink setGeodesic:toBool(geodesic)];
    }
    id width = data[@"width"];
    if (width) {
        [sink setWidth:(CGFloat)toFloat(width)];
    }
    id visible = data[@"visible"];
    if (visible) {
        [sink setVisible:toBool(visible)];
    }
    id zIndex = data[@"zIndex"];
    if (zIndex) {
        [sink setZIndex:toInt(zIndex)];
    }
}

static void interpretPolygonOptions(id json, id<FLTGoogleMapPolygonOptionsSink> sink,
                                     NSObject<FlutterPluginRegistrar>* registrar) {
    NSDictionary* data = json;
    
    id clickable = data[@"clickable"];
    if (clickable) {
        [sink setClickable:toBool(clickable)];
    }
    id fillColor = data[@"fillColor"];
    if (fillColor) {
        [sink setFillColor:colorFromInt(toInt(fillColor))];
    }
    id geodesic = data[@"geodesic"];
    if (geodesic) {
        [sink setGeodesic:toBool(geodesic)];
    }
    id holes = data[@"holes"];
    if (holes) {
        [sink setHoles:toHolesArray(holes)];
    }
    id points = data[@"points"];
    if (points) {
        [sink setPoints:toPath(points)];
    }
    id strokeColor = data[@"strokeColor"];
    if (strokeColor) {
        [sink setStrokeColor:colorFromInt(toInt(strokeColor))];
    }
    id strokeWidth = data[@"strokeWidth"];
    if (strokeWidth) {
        [sink setStrokeWidth:toFloat(strokeWidth)];
    }
    id visible = data[@"visible"];
    if (visible) {
        [sink setVisible:toBool(visible)];
    }
    id zIndex = data[@"zIndex"];
    if (zIndex) {
        [sink setZIndex:toInt(zIndex)];
    }
}
