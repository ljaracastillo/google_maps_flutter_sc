//
//  GoogleMapsCircleController.m
//  google_maps_flutter_sc
//
//  Created by CEISUFRO on 11-03-19.
//

#import "GoogleMapCircleController.h"

static uint64_t _nextCircleId = 0;

@implementation FLTGoogleMapCircleController{
    GMSCircle* _circle;
    GMSMapView* _mapView;
}

- (instancetype)initWithPosition:(CLLocationCoordinate2D)position mapView:(GMSMapView *)mapView{
    self = [super init];
    if(self){
        _circle = [GMSCircle circleWithPosition:position radius:0.0];
        _mapView = mapView;
        _circleId= [NSString stringWithFormat:@"%lld", _nextCircleId++];
        _circle.userData = @[_circleId, @(NO)];
    }
    return self;
}

#pragma mark - FTLGoogleMapCircleOptionsSink methods

-(void) setPosition:(CLLocationCoordinate2D)position{
    _circle.position = position;
}

-(void) setFillColor:(UIColor *)fillColor{
    _circle.fillColor = fillColor;
}

- (void)setClickable:(BOOL)clickable {
    _circle.tappable = clickable;
}

- (void)setRadius:(CLLocationDistance)radius {
    _circle.radius = radius;
}

- (void)setStrokeColor:(UIColor *)strokeColor {
    _circle.strokeColor = strokeColor;
}

- (void)setStrokeWidth:(float)strokeWidth {
    _circle.strokeWidth = strokeWidth;
}

- (void)setVisible:(BOOL)visible {
    _circle.map = visible ? _mapView : nil;
}

- (void)setZIndex:(int)zIndex {
    _circle.zIndex = zIndex;
}

@end
