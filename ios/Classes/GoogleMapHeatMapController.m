//
//  GoogleMapsCircleController.m
//  google_maps_flutter_sc
//
//  Created by CEISUFRO on 11-03-19.
//

#import "GoogleMapHeatMapController.h"

static uint64_t _nextHeatMapId = 0;

@implementation FLTGoogleMapHeatMapController{
    GMUHeatmapTileLayer* _heatMap;
    GMSMapView* _mapView;
}

-(instancetype) initWithData:(NSArray<CLLocation *> *)data mapView:(GMSMapView *)mapView{
    self = [super init];
    if(self){
        _heatMap = [GMUHeatmapTileLayer init];
        _heatMap.weightedData = [FLTGoogleMapHeatMapController wrapData:data];
        _mapView = mapView;
        _heatMapId= [NSString stringWithFormat:@"%lld", _nextHeatMapId++];
    }
    return self;
}
-(instancetype) initWithWeightedData:(NSArray<GMUWeightedLatLng *> *)weightedData mapView:(GMSMapView *)mapView{
    self = [super init];
    if(self){
        _heatMap = [GMUHeatmapTileLayer new];
        _heatMap.weightedData = weightedData;
        _mapView = mapView;
        _heatMapId = [NSString stringWithFormat:@"%lld", _nextHeatMapId++];
    }
    return self;
}
#pragma mark - FLTGoogleMapHeatMapController methods

+(NSArray<GMUWeightedLatLng *>*) wrapData:(NSArray<CLLocation * >* )data{
    NSMutableArray<GMUWeightedLatLng *>* weightedData = [NSMutableArray new];
    for (CLLocation* location in data) {
        GMUWeightedLatLng* weightedPoint = [[GMUWeightedLatLng alloc] initWithCoordinate:location.coordinate intensity:1.0];
        [weightedData addObject:weightedPoint];
    }
    return weightedData;
}

- (void)setData:(NSArray<CLLocation *>*)data {
    _heatMap.weightedData = [FLTGoogleMapHeatMapController wrapData:data];
    [self reloadView];
}

- (void)setGradient:(GMUGradient *)gradient {
    _heatMap.gradient = gradient;
    [self reloadView];
}

- (void)setOpacity:(float)opacity {
    _heatMap.opacity = opacity;
    [self reloadView];
}

- (void)setRadius:(CLLocationDistance)radius {
    _heatMap.radius = radius;
    [self reloadView];
}

- (void)setVisible:(BOOL)visible {
    _heatMap.map = visible ? _mapView : nil;
}

- (void)setWeightedData:(NSArray<GMUWeightedLatLng *> *)weightedData {
    _heatMap.weightedData = weightedData;
    [self reloadView];
}

- (void)reloadView{
    _heatMap.map = nil;
    _heatMap.map = _mapView;
}

@end
