// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import <Flutter/Flutter.h>
#import <Google-Maps-iOS-Utils/GMUHeatMapTileLayer.h>
#import <GoogleMaps/GoogleMaps.h>

// Defines heatmap UI options writable from Flutter.
@protocol FLTGoogleMapHeatMapOptionsSink
- (void)setData:(NSArray<CLLocation *>*)data;
- (void)setGradient:(GMUGradient *)gradient;
- (void)setOpacity:(float)opacity;
- (void)setRadius:(CLLocationDistance)radius;
- (void)setVisible:(BOOL)visible;
- (void)setWeightedData:(NSArray<GMUWeightedLatLng *>*)weightedData;
- (void)reloadView;
@end

// Defines heatmap controllable by Flutter.
@interface FLTGoogleMapHeatMapController : NSObject <FLTGoogleMapHeatMapOptionsSink>
@property(atomic, readonly) NSString* heatMapId;
- (instancetype)initWithData:(NSArray<CLLocation *>*)data mapView:(GMSMapView*)mapView;
- (instancetype)initWithWeightedData:(NSArray<GMUWeightedLatLng *>*)weightedData mapView:(GMSMapView*)mapView;
+(NSArray<GMUWeightedLatLng *>*) wrapData:(NSArray<CLLocation * >* )data;
@end
