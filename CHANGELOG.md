# Changelog

## 0.0.6

* **BREAKING CHANGE**: Updated library to ANDROID X.

## 0.0.5

* iOS part of 0.0.4 changes added and tested.

## 0.0.4

* [Android-Only] Polygon, polyline, circle & heatmap support added.
* Added android-maps-utils library.

## 0.0.3

* Don't export `dart:async`.
* Update the minimal required Flutter SDK version to one that supports embedding platform views.

## 0.0.2

* Initial developers preview release.
