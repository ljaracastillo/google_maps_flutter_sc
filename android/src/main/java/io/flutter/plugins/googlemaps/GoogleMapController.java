// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package io.flutter.plugins.googlemaps;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.platform.PlatformView;

import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.CREATED;
import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.DESTROYED;
import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.PAUSED;
import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.RESUMED;
import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.STARTED;
import static io.flutter.plugins.googlemaps.GoogleMapsPlugin.STOPPED;

/**
 * Controller of a single GoogleMaps MapView instance.
 */
final class GoogleMapController implements Application.ActivityLifecycleCallbacks, GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnCircleClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnPolygonClickListener,
        GoogleMap.OnPolylineClickListener, GoogleMapOptionsSink, MethodChannel.MethodCallHandler,
        OnMapReadyCallback, OnCircleTappedListener, OnMarkerTappedListener,
        OnPolygonTappedListener, OnPolylineTappedListener, PlatformView {
    private static final String TAG = "GoogleMapController";
    private final int id;
    private final AtomicInteger activityState;
    private final MethodChannel methodChannel;
    private final PluginRegistry.Registrar registrar;
    private final MapView mapView;
    private final Map<String, CircleController> circles;
    private final Map<String, HeatMapController> heatMaps;
    private final Map<String, MarkerController> markers;
    private final Map<String, PolylineController> polylines;
    private final Map<String, PolygonController> polygons;
    private GoogleMap googleMap;
    private boolean trackCameraPosition = false;
    private boolean myLocationEnabled = false;
    private boolean disposed = false;
    private final float density;
    private MethodChannel.Result mapReadyResult;
    private final int registrarActivityHashCode;
    private final Context context;

    GoogleMapController(int id, Context context, AtomicInteger activityState, PluginRegistry.Registrar registrar,
                        GoogleMapOptions options) {
        this.id = id;
        this.context = context;
        this.activityState = activityState;
        this.registrar = registrar;
        this.mapView = new MapView(context, options);
        this.circles = new HashMap<>();
        this.heatMaps = new HashMap<>();
        this.markers = new HashMap<>();
        this.polylines = new HashMap<>();
        this.polygons = new HashMap<>();
        this.density = context.getResources().getDisplayMetrics().density;
        methodChannel = new MethodChannel(registrar.messenger(), "plugins.flutter.io/google_maps_" + id);
        methodChannel.setMethodCallHandler(this);
        this.registrarActivityHashCode = registrar.activity().hashCode();
    }

    @Override
    public View getView() {
        return mapView;
    }

    void init() {
        switch (activityState.get()) {
            case STOPPED:
                mapView.onCreate(null);
                mapView.onStart();
                mapView.onResume();
                mapView.onPause();
                mapView.onStop();
                break;
            case PAUSED:
                mapView.onCreate(null);
                mapView.onStart();
                mapView.onResume();
                mapView.onPause();
                break;
            case RESUMED:
                mapView.onCreate(null);
                mapView.onStart();
                mapView.onResume();
                break;
            case STARTED:
                mapView.onCreate(null);
                mapView.onStart();
                break;
            case CREATED:
                mapView.onCreate(null);
                break;
            case DESTROYED:
                // Nothing to do, the activity has been completely destroyed.
                break;
            default:
                throw new IllegalArgumentException("Cannot interpret " + activityState.get() + " as an activity state");
        }
        registrar.activity().getApplication().registerActivityLifecycleCallbacks(this);
        mapView.getMapAsync(this);
    }

    private void moveCamera(CameraUpdate cameraUpdate) {
        googleMap.moveCamera(cameraUpdate);
    }

    private void animateCamera(CameraUpdate cameraUpdate) {
        googleMap.animateCamera(cameraUpdate);
    }

    private CameraPosition getCameraPosition() {
        return trackCameraPosition ? googleMap.getCameraPosition() : null;
    }

    private CircleBuilder newCircleBuilder() {
        return new CircleBuilder(this);
    }

    private HeatMapBuilder newHeatMapBuilder() {
        return new HeatMapBuilder(this);
    }

    private MarkerBuilder newMarkerBuilder() {
        return new MarkerBuilder(this);
    }

    private PolylineBuilder newPolylineBuilder() {
        return new PolylineBuilder(this);
    }

    private PolygonBuilder newPolygonBuilder() {
        return new PolygonBuilder(this);
    }

    Circle addCircle(CircleOptions circleOptions) {
        final Circle circle = googleMap.addCircle(circleOptions);
        circles.put(circle.getId(), new CircleController(circle, this));
        return circle;
    }

    private void removeCircle(String circleId) {
        final CircleController circleController = circles.remove(circleId);
        if (circleController != null) {
            circleController.remove();
        }
    }

    private CircleController circle(String circleId) {
        final CircleController circleController = circles.get(circleId);
        if (circleController == null) {
            throw new IllegalArgumentException("Unknown circle: " + circleId);
        }
        return circleController;
    }

    TileOverlay addHeatMap(HeatmapTileProvider heatmapTileProvider, TileOverlayOptions tileOverlayOptions) {
        final TileOverlay heatMapOverlay = googleMap.addTileOverlay(tileOverlayOptions.tileProvider(heatmapTileProvider));
        heatMaps.put(heatMapOverlay.getId(), new HeatMapController(heatmapTileProvider, heatMapOverlay));
        return heatMapOverlay;
    }

    private void removeHeatMap(String heatMapId) {
        final HeatMapController heatMapController = heatMaps.remove(heatMapId);
        if (heatMapController != null) {
            heatMapController.remove();
        }
    }

    private HeatMapController heatMap(String heatMapId) {
        final HeatMapController heatMapController = heatMaps.get(heatMapId);
        if (heatMapController == null) {
            throw new IllegalArgumentException("Unknown heat map: " + heatMapId);
        }
        return heatMapController;
    }

    Marker addMarker(MarkerOptions markerOptions, boolean consumesTapEvents) {
        final Marker marker = googleMap.addMarker(markerOptions);
        markers.put(marker.getId(), new MarkerController(marker, consumesTapEvents, this));
        return marker;
    }

    private void removeMarker(String markerId) {
        final MarkerController markerController = markers.remove(markerId);
        if (markerController != null) {
            markerController.remove();
        }
    }

    private MarkerController marker(String markerId) {
        final MarkerController marker = markers.get(markerId);
        if (marker == null) {
            throw new IllegalArgumentException("Unknown marker: " + markerId);
        }
        return marker;
    }

    Polyline addPolyline(PolylineOptions polylineOptions) {
        final Polyline polyline = googleMap.addPolyline(polylineOptions);
        polylines.put(polyline.getId(), new PolylineController(polyline, this));
        return polyline;
    }

    private void removePolyline(String polylineId) {
        final PolylineController polylineController = polylines.remove(polylineId);
        if (polylineController != null) {
            polylineController.remove();
        }
    }

    private PolylineController polyline(String polylineId) {
        final PolylineController polyline = polylines.get(polylineId);
        if (polyline == null) {
            throw new IllegalArgumentException("Unknown polyline: " + polylineId);
        }
        return polyline;
    }

    Polygon addPolygon(PolygonOptions polygonOptions) {
        final Polygon polygon = googleMap.addPolygon(polygonOptions);
        polygons.put(polygon.getId(), new PolygonController(polygon, this));
        return polygon;
    }

    private void removePolygon(String polygonId) {
        final PolygonController polygonController = polygons.remove(polygonId);
        if (polygonController != null) {
            polygonController.remove();
        }
    }

    private PolygonController polygon(String polygonId) {
        final PolygonController polygonController = polygons.get(polygonId);
        if (polygonController == null) {
            throw new IllegalArgumentException("Unknown polygon: " + polygonId);
        }
        return polygonController;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnInfoWindowClickListener(this);
        if (mapReadyResult != null) {
            mapReadyResult.success(null);
            mapReadyResult = null;
        }
        googleMap.setOnCameraMoveStartedListener(this);
        googleMap.setOnCameraMoveListener(this);
        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnCircleClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnPolygonClickListener(this);
        googleMap.setOnPolylineClickListener(this);
        updateMyLocationEnabled();
    }

    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        switch (call.method) {
            case "map#waitForMap":
                if (googleMap != null) {
                    result.success(null);
                    return;
                }
                mapReadyResult = result;
                break;
            case "map#update": {
                Convert.interpretGoogleMapOptions(call.argument("options"), this);
                result.success(Convert.toJson(getCameraPosition()));
                break;
            }
            case "camera#move": {
                final CameraUpdate cameraUpdate = Convert.toCameraUpdate(call.argument("cameraUpdate"), density);
                moveCamera(cameraUpdate);
                result.success(null);
                break;
            }
            case "camera#animate": {
                final CameraUpdate cameraUpdate = Convert.toCameraUpdate(call.argument("cameraUpdate"), density);
                animateCamera(cameraUpdate);
                result.success(null);
                break;
            }
            case "circle#add": {
                final CircleBuilder circleBuilder = newCircleBuilder();
                Convert.interpretCircleOptions(call.argument("options"), circleBuilder);
                final String circleId = circleBuilder.build();
                result.success(circleId);
                break;
            }
            case "circle#remove": {
                final String circleId = call.argument("circle");
                removeCircle(circleId);
                result.success(null);
                break;
            }
            case "circle#update": {
                final String circleId = call.argument("circle");
                final CircleController circle = circle(circleId);
                Convert.interpretCircleOptions(call.argument("options"), circle);
                result.success(null);
                break;
            }
            case "heatMap#add": {
                final HeatMapBuilder heatMapBuilder = newHeatMapBuilder();
                Convert.interpretHeatMapOptions(call.argument("options"), heatMapBuilder);
                final String heatMapId = heatMapBuilder.build();
                result.success(heatMapId);
                break;
            }
            case "heatMap#remove": {
                final String heatMap = call.argument("heatMap");
                removeHeatMap(heatMap);
                result.success(null);
                break;
            }
            case "heatMap#update": {
                final String heatMapId = call.argument("heatMap");
                final HeatMapController heatMap = heatMap(heatMapId);
                Convert.interpretHeatMapOptions(call.argument("options"), heatMap);
                result.success(null);
                break;
            }
            case "marker#add": {
                final MarkerBuilder markerBuilder = newMarkerBuilder();
                Convert.interpretMarkerOptions(call.argument("options"), markerBuilder);
                final String markerId = markerBuilder.build();
                result.success(markerId);
                break;
            }
            case "marker#remove": {
                final String markerId = call.argument("marker");
                removeMarker(markerId);
                result.success(null);
                break;
            }
            case "marker#update": {
                final String markerId = call.argument("marker");
                final MarkerController marker = marker(markerId);
                Convert.interpretMarkerOptions(call.argument("options"), marker);
                result.success(null);
                break;
            }
            case "polyline#add": {
                final PolylineBuilder polylineBuilder = newPolylineBuilder();
                Convert.interpretPolylineOptions(call.argument("options"), polylineBuilder);
                final String polylineId = polylineBuilder.build();
                result.success(polylineId);
                break;
            }
            case "polyline#remove": {
                final String polylineId = call.argument("polyline");
                removePolyline(polylineId);
                result.success(null);
                break;
            }
            case "polyline#update": {
                final String polylineId = call.argument("polyline");
                final PolylineController polyline = polyline(polylineId);
                Convert.interpretPolylineOptions(call.argument("options"), polyline);
                result.success(null);
                break;
            }
            case "polygon#add": {
                final PolygonBuilder polygonBuilder = newPolygonBuilder();
                Convert.interpretPolygonOptions(call.argument("options"), polygonBuilder);
                final String polygonId = polygonBuilder.build();
                result.success(polygonId);
                break;
            }
            case "polygon#remove": {
                final String polygonId = call.argument("polygon");
                removePolygon(polygonId);
                result.success(null);
                break;
            }
            case "polygon#update": {
                final String polygonId = call.argument("polygon");
                final PolygonController polygon = polygon(polygonId);
                Convert.interpretPolygonOptions(call.argument("options"), polygon);
                result.success(null);
                break;
            }
            default:
                result.notImplemented();
        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        final Map<String, Object> arguments = new HashMap<>(2);
        boolean isGesture = reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE;
        arguments.put("isGesture", isGesture);
        methodChannel.invokeMethod("camera#onMoveStarted", arguments);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("marker", marker.getId());
        methodChannel.invokeMethod("infoWindow#onTap", arguments);
    }

    @Override
    public void onCameraMove() {
        if (!trackCameraPosition) {
            return;
        }
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("position", Convert.toJson(googleMap.getCameraPosition()));
        methodChannel.invokeMethod("camera#onMove", arguments);
    }

    @Override
    public void onCameraIdle() {
        methodChannel.invokeMethod("camera#onIdle", Collections.singletonMap("map", id));
    }

    @Override
    public void onCircleClick(Circle circle) {
        final CircleController circleController = circles.get(circle.getId());
        if (circleController != null) {
            circleController.onTap();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        final MarkerController markerController = markers.get(marker.getId());
        return (markerController != null && markerController.onTap());
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        final PolylineController polylineController = polylines.get(polyline.getId());
        if (polylineController != null) {
            polylineController.onTap();
        }
    }

    @Override
    public void onPolygonClick(Polygon polygon) {
        final PolygonController polygonController = polygons.get(polygon.getId());
        if (polygonController != null) {
            polygonController.onTap();
        }
    }

    @Override
    public void onCircleTapped(Circle circle) {
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("circle", circle.getId());
        methodChannel.invokeMethod("circle#onTap", arguments);
    }

    @Override
    public void onMarkerTapped(Marker marker) {
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("marker", marker.getId());
        methodChannel.invokeMethod("marker#onTap", arguments);
    }

    @Override
    public void onPolylineTapped(Polyline polyline) {
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("polyline", polyline.getId());
        methodChannel.invokeMethod("polyline#onTap", arguments);
    }

    @Override
    public void onPolygonTapped(Polygon polygon) {
        final Map<String, Object> arguments = new HashMap<>(2);
        arguments.put("polygon", polygon.getId());
        methodChannel.invokeMethod("polygon#onTap", arguments);
    }

    @Override
    public void dispose() {
        if (disposed) {
            return;
        }
        disposed = true;
        mapView.onDestroy();
        registrar.activity().getApplication().unregisterActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onStart();
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onResume();
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onPause();
    }

    @Override
    public void onActivityStopped(Activity activity) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onStop();
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (disposed || activity.hashCode() != registrarActivityHashCode) {
            return;
        }
        mapView.onDestroy();
    }

    // GoogleMapOptionsSink methods

    @Override
    public void setCameraPosition(CameraPosition position) {
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    @Override
    public void setCameraTargetBounds(LatLngBounds bounds) {
        googleMap.setLatLngBoundsForCameraTarget(bounds);
    }

    @Override
    public void setCompassEnabled(boolean compassEnabled) {
        googleMap.getUiSettings().setCompassEnabled(compassEnabled);
    }

    @Override
    public void setMapType(int mapType) {
        googleMap.setMapType(mapType);
    }

    @Override
    public void setTrackCameraPosition(boolean trackCameraPosition) {
        this.trackCameraPosition = trackCameraPosition;
    }

    @Override
    public void setRotateGesturesEnabled(boolean rotateGesturesEnabled) {
        googleMap.getUiSettings().setRotateGesturesEnabled(rotateGesturesEnabled);
    }

    @Override
    public void setScrollGesturesEnabled(boolean scrollGesturesEnabled) {
        googleMap.getUiSettings().setScrollGesturesEnabled(scrollGesturesEnabled);
    }

    @Override
    public void setTiltGesturesEnabled(boolean tiltGesturesEnabled) {
        googleMap.getUiSettings().setTiltGesturesEnabled(tiltGesturesEnabled);
    }

    @Override
    public void setMinMaxZoomPreference(Float min, Float max) {
        googleMap.resetMinMaxZoomPreference();
        if (min != null) {
            googleMap.setMinZoomPreference(min);
        }
        if (max != null) {
            googleMap.setMaxZoomPreference(max);
        }
    }

    @Override
    public void setZoomGesturesEnabled(boolean zoomGesturesEnabled) {
        googleMap.getUiSettings().setZoomGesturesEnabled(zoomGesturesEnabled);
    }

    @Override
    public void setMyLocationEnabled(boolean myLocationEnabled) {
        if (this.myLocationEnabled == myLocationEnabled) {
            return;
        }
        this.myLocationEnabled = myLocationEnabled;
        if (googleMap != null) {
            updateMyLocationEnabled();
        }
    }

    private void updateMyLocationEnabled() {
        if (hasLocationPermission()) {
            googleMap.setMyLocationEnabled(myLocationEnabled);
        } else {
            // TODO(amirh): Make the options update fail.
            // https://github.com/flutter/flutter/issues/24327
            Log.e(TAG, "Cannot enable MyLocation layer as location permissions are not granted");
        }
    }

    private boolean hasLocationPermission() {
        return checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private int checkSelfPermission(String permission) {
        if (permission == null) {
            throw new IllegalArgumentException("permission is null");
        }
        return context.checkPermission(permission, android.os.Process.myPid(), android.os.Process.myUid());
    }


}
