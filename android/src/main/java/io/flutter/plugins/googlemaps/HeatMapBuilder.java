package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.List;

class HeatMapBuilder implements HeatMapOptionsSink {
    private final GoogleMapController mapController;
    private final HeatmapTileProvider.Builder heatMapOptions;
    private final TileOverlayOptions tileOverlayOptions;

    HeatMapBuilder(GoogleMapController mapController) {
        this.mapController = mapController;
        this.heatMapOptions = new HeatmapTileProvider.Builder();
        this.tileOverlayOptions = new TileOverlayOptions();
    }

    String build() {
        final TileOverlay heatMapOverlay = mapController.addHeatMap(heatMapOptions.build(), tileOverlayOptions);
        return heatMapOverlay.getId();
    }

    @Override
    public void setData(List<LatLng> data) {
        heatMapOptions.data(data);
    }

    @Override
    public void setGradient(Gradient gradient) {
        heatMapOptions.gradient(gradient);
    }

    @Override
    public void setOpacity(double opacity) {
        heatMapOptions.opacity(opacity);
    }

    @Override
    public void setRadius(int radius) {
        heatMapOptions.radius(radius);
    }

    @Override
    public void setVisible(boolean visible) {
        tileOverlayOptions.visible(visible);
    }

    @Override
    public void setWeightedData(List<WeightedLatLng> weightedData) {
        heatMapOptions.weightedData(weightedData);
    }
}
