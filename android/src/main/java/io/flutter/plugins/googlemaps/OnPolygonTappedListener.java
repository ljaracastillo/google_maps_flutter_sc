package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.Polygon;

interface OnPolygonTappedListener {
  void onPolygonTapped(Polygon polygon);
}
