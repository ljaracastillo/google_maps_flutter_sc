package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;

import java.util.List;

/**
 * Receiver of circle configuration options.
 */
interface CircleOptionsSink {
    void setCenter(LatLng center);

    void setRadius(double radius);

    void setFillColor(int fillColor);

    void setStrokeColor(int strokeColor);

    void setStrokePattern(List<PatternItem> strokePattern);

    void setStrokeWidth(float strokeWidth);

    void setClickable(boolean clickable);

    void setVisible(boolean visible);

    void setZIndex(float zIndex);
}
