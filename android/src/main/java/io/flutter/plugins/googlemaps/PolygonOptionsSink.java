package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;

import java.util.List;

/**
 * Receiver of Polygon configuration options.
 */
interface PolygonOptionsSink {

    void setClickable(boolean clickable);

    void setFillColor(int fillColor);

    void setGeodesic(boolean geodesic);

    void setStrokeJointType(int strokeJointType);

    void setStrokePattern(List<PatternItem> strokePattern);

    void setPoints(List<LatLng> points);

    void setHoles(List<List<LatLng>> holes);

    void setVisible(boolean visible);

    void setStrokeWidth(float strokeWidth);

    void setStrokeColor(int strokeColor);

    void setZIndex(float zIndex);
}
