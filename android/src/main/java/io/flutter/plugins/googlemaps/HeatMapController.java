package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.List;

/**
 * Controller of a single Polygon on the map.
 */
class HeatMapController implements HeatMapOptionsSink {
    private final HeatmapTileProvider heatmapTileProvider;
    private final TileOverlay heatMapOverlay;

    HeatMapController(HeatmapTileProvider heatmapTileProvider,
                      TileOverlay heatMapOverlay) {
        this.heatmapTileProvider = heatmapTileProvider;
        this.heatMapOverlay = heatMapOverlay;
    }

    void remove() {
        heatMapOverlay.remove();
    }


    @Override
    public void setData(List<LatLng> data) {
        heatmapTileProvider.setData(data);
        heatMapOverlay.clearTileCache();
    }

    @Override
    public void setGradient(Gradient gradient) {
        heatmapTileProvider.setGradient(gradient);
        heatMapOverlay.clearTileCache();
    }

    @Override
    public void setOpacity(double opacity) {
        heatmapTileProvider.setOpacity(opacity);
        heatMapOverlay.clearTileCache();
    }

    @Override
    public void setRadius(int radius) {
        heatmapTileProvider.setRadius(radius);
        heatMapOverlay.clearTileCache();
    }

    @Override
    public void setVisible(boolean visible) {
        heatMapOverlay.setVisible(visible);
        heatMapOverlay.clearTileCache();
    }

    @Override
    public void setWeightedData(List<WeightedLatLng> weightedData) {
        heatmapTileProvider.setWeightedData(weightedData);
        heatMapOverlay.clearTileCache();
    }
}
