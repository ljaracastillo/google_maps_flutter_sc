package io.flutter.plugins.googlemaps;

import com.google.android.gms.maps.model.Cap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PatternItem;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.util.List;

/**
 * Receiver of Polyline configuration options.
 */
interface HeatMapOptionsSink {

    void setData(List<LatLng> data);

    void setGradient(Gradient gradient);

    void setOpacity(double opacity);

    void setRadius(int radius);

    void setVisible(boolean visible);

    void setWeightedData(List<WeightedLatLng> weightedData);
}
