part of google_maps_flutter_sc;

class _Codex {
  static dynamic markerToJson(MarkerOptions marker) {
    final Map<String, dynamic> json = <String, dynamic>{};

    void addIfPresent(String fieldName, dynamic value) {
      if (value != null) {
        json[fieldName] = value;
      }
    }

    addIfPresent('alpha', marker.alpha);
    addIfPresent('anchor', _offsetToJson(marker.anchor));
    addIfPresent('consumeTapEvents', marker.consumeTapEvents);
    addIfPresent('draggable', marker.draggable);
    addIfPresent('flat', marker.flat);
    addIfPresent('icon', marker.icon?._toJson());
    addIfPresent('infoWindowAnchor', _offsetToJson(marker.infoWindowAnchor));
    addIfPresent('infoWindowText', marker.infoWindowText?._toJson());
    addIfPresent('position', marker.position?._toJson());
    addIfPresent('rotation', marker.rotation);
    addIfPresent('visible', marker.visible);
    addIfPresent('zIndex', marker.zIndex);
    return json;
  }

  static dynamic polylineToJson(PolylineOptions polyline) {
    final Map<String, dynamic> json = <String, dynamic>{};

    void addIfPresent(String fieldName, dynamic value) {
      if (value != null) {
        json[fieldName] = value;
      }
    }

    addIfPresent('consumeTapEvents', polyline.consumeTapEvents);
    addIfPresent('color', polyline.color);
    addIfPresent('endCap', polyline.endCap?._toJson());
    addIfPresent('geodesic', polyline.geodesic);
    addIfPresent('jointType', polyline.jointType);
    addIfPresent('startCap', polyline.startCap?._toJson());
    addIfPresent('visible', polyline.visible);
    addIfPresent('width', polyline.width);
    addIfPresent('zIndex', polyline.zIndex);

    if (polyline.points != null)
      json['points'] = _pointsToJson(polyline.points);

    json['pattern'] = _patternToJson(polyline.pattern);

    return json;
  }

  static dynamic polygonToJson(PolygonOptions polygon) {
    final Map<String, dynamic> json = <String, dynamic>{};

    void addIfPresent(String fieldName, dynamic value) {
      if (value != null) {
        json[fieldName] = value;
      }
    }

    addIfPresent('fillColor', polygon.fillColor);
    addIfPresent('visible', polygon.visible);
    addIfPresent('clickable', polygon.clickable);
    addIfPresent('geodesic', polygon.geodesic);
    addIfPresent('strokeJointType', polygon.strokeJointType);
    addIfPresent('strokeWidth', polygon.strokeWidth);
    addIfPresent('strokeColor', polygon.strokeColor);
    addIfPresent('zIndex', polygon.zIndex);

    if (polygon.points != null) json['points'] = _pointsToJson(polygon.points);
    if (polygon.holes != null) json['holes'] = _holesToJson(polygon.holes);
    json['strokePattern'] = _patternToJson(polygon.strokePattern);

    return json;
  }

  static dynamic circleToJson(CircleOptions circle) {
    final Map<String, dynamic> json = <String, dynamic>{};

    void addIfPresent(String fieldName, dynamic value) {
      if (value != null) {
        json[fieldName] = value;
      }
    }

    if (circle.center != null) addIfPresent('center', circle.center._toJson());
    addIfPresent('radius', circle.radius);
    addIfPresent('fillColor', circle.fillColor);
    addIfPresent('strokeColor', circle.strokeColor);
    addIfPresent('strokeWidth', circle.strokeWidth);
    addIfPresent('clickable', circle.clickable);
    addIfPresent('visible', circle.visible);
    addIfPresent('zIndex', circle.zIndex);

    json['strokePattern'] = _patternToJson(circle.strokePattern);

    return json;
  }

  static dynamic heatMapToJson(HeatMapOptions heatMap) {
    final Map<String, dynamic> json = <String, dynamic>{};

    void addIfPresent(String fieldName, dynamic value) {
      if (value != null) {
        json[fieldName] = value;
      }
    }
    
    addIfPresent('radius', heatMap.radius);
    addIfPresent('opacity', heatMap.opacity);
    addIfPresent('visible', heatMap.visible);
    if (heatMap.data != null) json['data'] = _pointsToJson(heatMap.data);
    if (heatMap.gradient != null)
      json['gradient'] = _gradientToJson(heatMap.gradient);
    if (heatMap.weightedData != null)
      json['weightedData'] = _weightedPointToJson(heatMap.weightedData);
    return json;
  }

  static dynamic _pointsToJson(List<LatLng> points) {
    final List<dynamic> result = <dynamic>[];
    for (final LatLng point in points) {
      result.add(point._toJson());
    }
    return result;
  }

  static dynamic _gradientToJson(Gradient gradient) {
    final Map<String, dynamic> json = <String, dynamic>{};
    final List<int> colors = [];
    for (Color color in gradient.colors) {
      colors.add(color.value);
    }
    json['colors'] = colors;
    json['stops'] = gradient.stops;
    return json;
  }

  static dynamic _weightedPointToJson(List<WeightedLatLng> weightedData) {
    final List<dynamic> result = <dynamic>[];
    for (final WeightedLatLng weightedPoint in weightedData) {
      final Map<String, dynamic> json = <String, dynamic>{};
      json['point'] = weightedPoint.point._toJson();
      json['intensity'] = weightedPoint.intensity;
      result.add(json);
    }
    return result;
  }

  static dynamic _holesToJson(List<List<LatLng>> holes) {
    final List<List<dynamic>> result = [];
    for (final List<LatLng> hole in holes) {
      List holeList = _pointsToJson(hole);
      if (holeList.isNotEmpty) result.add(holeList);
    }
    return result;
  }

  static dynamic _patternToJson(List<PatternItem> strokePattern) {
    if (strokePattern == null) {
      return null;
    }

    final List<dynamic> result = <dynamic>[];
    for (final PatternItem patternItem in strokePattern) {
      if (patternItem != null) {
        result.add(patternItem._toJson());
      }
    }
    return result;
  }
}
