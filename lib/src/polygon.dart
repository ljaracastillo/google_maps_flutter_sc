part of google_maps_flutter_sc;

/// A polygon on the earth's surface. A polygon can be convex or concave,
/// it may span the 180 meridian and it can have holes that are not filled in.
class Polygon {
  Polygon(this._id, this._options);

  String _id;
  String get id => _id;

  PolygonOptions _options;

  PolygonOptions get options => _options;
}

class PolygonOptions {
  const PolygonOptions({
    this.points,
    this.holes,
    this.clickable,
    this.fillColor,
    this.geodesic,
    this.strokeColor,
    this.strokeJointType,
    this.strokeWidth,
    this.zIndex,
    this.visible,
    this.strokePattern,
  });

  final List<LatLng> points;

  /// A hole is a region inside the polygon that is not filled.
  /// A hole is specified in exactly the same way as the outline.
  /// A hole must be fully contained within the outline.
  /// Multiple holes can be specified, however overlapping holes are not supported.
  final List<List<LatLng>> holes;

  /// If you want to handle events fired when the user clicks the polygon, 
  /// set this property to true. You can change this value at any time. 
  /// The default is false. If this property is set to true, your app will 
  /// receive notifications to the [GoogleMapController#onPolygonTapped].
  final bool clickable;

  /// Indicates whether the segments of the polygon should be drawn as geodesics, 
  /// as opposed to straight lines on the Mercator projection. A geodesic is the 
  /// shortest path between two points on the Earth's surface. The geodesic 
  /// curve is constructed assuming the Earth is a sphere
  final bool geodesic;

  /// Indicates if the polygon is visible or invisible, i.e., whether it is drawn 
  /// on the map. An invisible polygon is not drawn, but retains all 
  /// of its other properties. The default is true, i.e., visible.
  final bool visible;

  /// Fill color in ARGB format, the same format used by Color. 
  /// The default value is transparent (0x00000000). If the polygon 
  /// geometry is not specified correctly (see above for Outline and Holes), then no fill will be drawn.
  final int fillColor;

  /// Line segment color in ARGB format, the same format used by 
  /// Color. The default value is black (0xff000000).
  final int strokeColor;

  /// The joint type defines the shape to be used when joining 
  /// adjacent line segments at all vertices of the polygon's outline. 
  /// See [JointType] for supported joint types. The default value is [JointType.mitered].
  final int strokeJointType;

  /// Solid (default, represented by null) or a sequence of [PatternItem] 
  /// objects to be repeated along the polygon's outline. Available [PatternItem] types: 
  /// * Gap (defined by gap length in pixels), 
  /// * Dash (defined by stroke width and dash length in pixels)
  /// * Dot (circular, centered on the polygon's outline, diameter defined by stroke width in pixels).
  final List<PatternItem> strokePattern;

  /// Line segment width in screen pixels. The width is constant 
  /// and independent of the camera's zoom level. The default value is 10.
  final double strokeWidth;

  /// The order in which this polygon is drawn with respect to other overlays, 
  /// including Polylines, Circles, GroundOverlays and TileOverlays,
  /// but not Markers. An overlay with a larger z-index is drawn over 
  /// overlays with smaller z-indices. The order of overlays with the same 
  /// z-index value is arbitrary. The default is 0.
  final double zIndex;

  static const PolygonOptions defaultOptions = PolygonOptions(
    clickable: false,
    fillColor: 0x00000000,
    strokeColor: 0xff000000,
    geodesic: false,
    strokeJointType: 0,
    strokePattern: null,
    points: null,
    holes: null,
    visible: true,
    strokeWidth: 10,
    zIndex: 0.0,
  );

  /// Creates a new options object whose values are the same as this instance,
  /// unless overwritten by the specified [changes].
  ///
  /// Returns this instance, if [changes] is null.
  PolygonOptions copyWith(PolygonOptions changes) {
    if (changes == null) {
      return this;
    }
    return PolygonOptions(
      fillColor: changes.fillColor ?? fillColor,
      strokeJointType: changes.strokeJointType ?? strokeJointType,
      strokePattern: changes.strokePattern,
      strokeWidth: changes.strokeWidth ?? strokeWidth,
      strokeColor: changes.strokeColor ?? strokeColor,
      points: changes.points ?? points,
      holes: changes.holes ?? holes,
      visible: changes.visible ?? visible,
      clickable: changes.clickable ?? clickable,
      geodesic: changes.geodesic ?? geodesic,
      zIndex: changes.zIndex ?? zIndex,
    );
  }
}
