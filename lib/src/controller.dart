// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

part of google_maps_flutter_sc;

/// Controller for a single GoogleMap instance running on the host platform.
///
/// Change listeners are notified upon changes to any of
///
/// * the [options] property
/// * the collection of [Marker]s added to this map
/// * the [isCameraMoving] property
/// * the [cameraPosition] property
///
/// Listeners are notified after changes have been applied on the platform side.
///
/// Marker tap events can be received by adding callbacks to [onMarkerTapped].
class GoogleMapController extends ChangeNotifier {
  GoogleMapController._(
      this._id, GoogleMapOptions options, MethodChannel channel)
      : assert(_id != null),
        assert(options != null),
        assert(options.cameraPosition != null),
        assert(channel != null),
        _channel = channel {
    if (options.trackCameraPosition) {
      _cameraPosition = options.cameraPosition;
    }
    _channel.setMethodCallHandler(_handleMethodCall);
    _options = GoogleMapOptions.defaultOptions.copyWith(options);
  }

  static Future<GoogleMapController> init(
      int id, GoogleMapOptions options) async {
    assert(id != null);
    assert(options != null);
    assert(options.cameraPosition != null);
    final MethodChannel channel =
        MethodChannel('plugins.flutter.io/google_maps_$id');
    await channel.invokeMethod('map#waitForMap');
    return GoogleMapController._(id, options, channel);
  }

  final MethodChannel _channel;

  /// Callbacks to receive tap events for circle placed on this map.
  final ArgumentCallbacks<Circle> onCircleTapped = ArgumentCallbacks<Circle>();

  /// Callbacks to receive tap events for markers placed on this map.
  final ArgumentCallbacks<Marker> onMarkerTapped = ArgumentCallbacks<Marker>();

  /// Callbacks to receive tap events for polylines placed on this map.
  final ArgumentCallbacks<Polyline> onPolylineTapped =
      ArgumentCallbacks<Polyline>();

  /// Callbacks to receive tap events for polygons placed on this map.
  final ArgumentCallbacks<Polygon> onPolygonTapped =
      ArgumentCallbacks<Polygon>();

  /// Callbacks to receive tap events for info windows on markers
  final ArgumentCallbacks<Marker> onInfoWindowTapped =
      ArgumentCallbacks<Marker>();

  /// The configuration options most recently applied via controller
  /// initialization or [updateMapOptions].
  GoogleMapOptions get options => _options;
  GoogleMapOptions _options;

  /// The current set of circles on this map.
  ///
  /// The returned set will be a detached snapshot of the circles collection.
  Set<Circle> get circles => Set<Circle>.from(_circles.values);
  final Map<String, Circle> _circles = <String, Circle>{};

  /// The current set of heat maps on this map.
  ///
  /// The returned set will be a detached snapshot of the circles collection.
  Set<HeatMap> get heatMaps => Set<HeatMap>.from(_heatMaps.values);
  final Map<String, HeatMap> _heatMaps = <String, HeatMap>{};

  /// The current set of markers on this map.
  ///
  /// The returned set will be a detached snapshot of the markers collection.
  Set<Marker> get markers => Set<Marker>.from(_markers.values);
  final Map<String, Marker> _markers = <String, Marker>{};

  /// The current set of polylines on this map.
  ///
  /// The returned set will be a detached snapshot of the polylines collection.
  Set<Polyline> get polylines => Set<Polyline>.from(_polylines.values);
  final Map<String, Polyline> _polylines = <String, Polyline>{};

  /// The current set of polygons on this map.
  ///
  /// The returned set will be a detached snapshot of the polygons collection.
  Set<Polygon> get polygons => Set<Polygon>.from(_polygons.values);
  final Map<String, Polygon> _polygons = <String, Polygon>{};

  /// True if the map camera is currently moving.
  bool get isCameraMoving => _isCameraMoving;
  bool _isCameraMoving = false;

  /// Returns the most recent camera position reported by the platform side.
  /// Will be null, if camera position tracking is not enabled via
  /// [GoogleMapOptions].
  CameraPosition get cameraPosition => _cameraPosition;
  CameraPosition _cameraPosition;

  final int _id;

  Future<dynamic> _handleMethodCall(MethodCall call) async {
    switch (call.method) {
      case 'infoWindow#onTap':
        final String markerId = call.arguments['marker'];
        final Marker marker = _markers[markerId];
        if (marker != null) {
          onInfoWindowTapped(marker);
        }
        break;
      case 'circle#onTap':
        final String circleId = call.arguments['circle'];
        final Circle circle = _circles[circleId];
        if (circle != null) {
          onCircleTapped(circle);
        }
        break;
      case 'marker#onTap':
        final String markerId = call.arguments['marker'];
        final Marker marker = _markers[markerId];
        if (marker != null) {
          onMarkerTapped(marker);
        }
        break;
      case 'polyline#onTap':
        final String polylineId = call.arguments['polyline'];
        final Polyline polyline = _polylines[polylineId];
        if (polyline != null) {
          onPolylineTapped(polyline);
        }
        break;
      case 'polygon#onTap':
        final String polygonId = call.arguments['polygon'];
        final Polygon polygon = _polygons[polygonId];
        if (polygon != null) {
          onPolygonTapped(polygon);
        }
        break;
      case 'camera#onMoveStarted':
        _isCameraMoving = true;
        notifyListeners();
        break;
      case 'camera#onMove':
        _cameraPosition = CameraPosition._fromJson(call.arguments['position']);
        notifyListeners();
        break;
      case 'camera#onIdle':
        _isCameraMoving = false;
        notifyListeners();
        break;
      default:
        throw MissingPluginException();
    }
  }

  /// Updates configuration options of the map user interface.
  ///
  /// Change listeners are notified once the update has been made on the
  /// platform side.
  ///
  /// The returned [Future] completes after listeners have been notified.
  Future<void> updateMapOptions(GoogleMapOptions changes) async {
    assert(changes != null);
    final dynamic json = await _channel.invokeMethod(
      'map#update',
      <String, dynamic>{
        'options': changes._toJson(),
      },
    );
    _options = _options.copyWith(changes);
    _cameraPosition = CameraPosition._fromJson(json);
    notifyListeners();
  }

  /// Starts an animated change of the map camera position.
  ///
  /// The returned [Future] completes after the change has been started on the
  /// platform side.
  Future<void> animateCamera(CameraUpdate cameraUpdate) async {
    await _channel.invokeMethod('camera#animate', <String, dynamic>{
      'cameraUpdate': cameraUpdate._toJson(),
    });
  }

  /// Changes the map camera position.
  ///
  /// The returned [Future] completes after the change has been made on the
  /// platform side.
  Future<void> moveCamera(CameraUpdate cameraUpdate) async {
    await _channel.invokeMethod('camera#move', <String, dynamic>{
      'cameraUpdate': cameraUpdate._toJson(),
    });
  }

  /// Adds a circle to the map, configured using the specified custom [options].
  ///
  /// Change listeners are notified once the circle has been added on the
  /// platform side.
  ///
  /// The returned [Future] completes with the added circle once listeners have
  /// been notified.
  Future<Circle> addCircle(CircleOptions options) async {
    final CircleOptions effectiveOptions =
        CircleOptions.defaultOptions.copyWith(options);
    final String circleId = await _channel.invokeMethod(
      'circle#add',
      <String, dynamic>{
        'options': _Codex.circleToJson(effectiveOptions),
      },
    );
    final Circle circle = Circle(circleId, effectiveOptions);
    _circles[circleId] = circle;
    notifyListeners();
    return circle;
  }

  /// Updates the specified [circle] with the given [changes]. The circle must
  /// be a current member of the [circles] set.
  ///
  /// Change listeners are notified once the circle has been updated on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> updateCircle(Circle circle, CircleOptions changes) async {
    assert(circle != null);
    assert(_circles[circle._id] == circle);
    assert(changes != null);
    await _channel.invokeMethod('circle#update', <String, dynamic>{
      'circle': circle._id,
      'options': _Codex.circleToJson(changes),
    });
    circle._options = circle._options.copyWith(changes);
    notifyListeners();
  }

  /// Removes the specified [circle] from the map. The circle must be a current
  /// member of the [circle] set.
  ///
  /// Change listeners are notified once the circle has been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> removeCircle(Circle circle) async {
    assert(circle != null);
    assert(_circles[circle._id] == circle);
    await _removeCircle(circle._id);
    notifyListeners();
  }

  /// Removes all [circles] from the map.
  ///
  /// Change listeners are notified once all circles have been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> clearCircles() async {
    assert(_circles != null);
    final List<String> circlesIds = List<String>.from(_circles.keys);
    for (String id in circlesIds) {
      await _removeCircle(id);
    }
    notifyListeners();
  }

  /// Helper method to remove a single circle from the map. Consumed by
  /// [removeCircle] and [clearCircles].
  ///
  /// The returned [Future] completes once the circle has been removed from
  /// [_circles].
  Future<void> _removeCircle(String id) async {
    await _channel.invokeMethod('circle#remove', <String, dynamic>{
      'circle': id,
    });
    _circles.remove(id);
  }

  /// Adds a circle to the map, configured using the specified custom [options].
  ///
  /// Change listeners are notified once the circle has been added on the
  /// platform side.
  ///
  /// The returned [Future] completes with the added circle once listeners have
  /// been notified.
  Future<HeatMap> addHeatMap(HeatMapOptions options) async {
    final HeatMapOptions effectiveOptions =
        HeatMapOptions.defaultOptions.copyWith(options);
    final String heatMapId = await _channel.invokeMethod(
      'heatMap#add',
      <String, dynamic>{
        'options': _Codex.heatMapToJson(effectiveOptions),
      },
    );
    final HeatMap heatMap = HeatMap(heatMapId, effectiveOptions);
    _heatMaps[heatMapId] = heatMap;
    notifyListeners();
    return heatMap;
  }

  /// Updates the specified [heatMap] with the given [changes]. The circle must
  /// be a current member of the [circles] set.
  ///
  /// Change listeners are notified once the circle has been updated on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> updateHeatMap(HeatMap heatMap, HeatMapOptions changes) async {
    assert(heatMap != null);
    assert(_heatMaps[heatMap._id] == heatMap);
    assert(changes != null);
    await _channel.invokeMethod('heatMap#update', <String, dynamic>{
      'heatMap': heatMap._id,
      'options': _Codex.heatMapToJson(changes),
    });
    heatMap._options = heatMap._options.copyWith(changes);
    notifyListeners();
  }

  /// Removes the specified [heatMap] from the map. The circle must be a current
  /// member of the [heatMap] set.
  ///
  /// Change listeners are notified once the circle has been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> removeHeatMap(HeatMap heatMap) async {
    assert(heatMap != null);
    assert(_heatMaps[heatMap._id] == heatMap);
    await _removeHeatMap(heatMap._id);
    notifyListeners();
  }

  /// Removes all [heatMaps] from the map.
  ///
  /// Change listeners are notified once all circles have been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> clearHeatMap() async {
    assert(_heatMaps != null);
    final List<String> heatMapsIds = List<String>.from(_heatMaps.keys);
    for (String id in heatMapsIds) {
      await _removeHeatMap(id);
    }
    notifyListeners();
  }

  /// Helper method to remove a single circle from the map. Consumed by
  /// [removeCircle] and [clearCircles].
  ///
  /// The returned [Future] completes once the circle has been removed from
  /// [_circles].
  Future<void> _removeHeatMap(String id) async {
    await _channel.invokeMethod('heatMap#remove', <String, dynamic>{
      'heatMap': id,
    });
    _circles.remove(id);
  }

  /// Adds a marker to the map, configured using the specified custom [options].
  ///
  /// Change listeners are notified once the marker has been added on the
  /// platform side.
  ///
  /// The returned [Future] completes with the added marker once listeners have
  /// been notified.
  Future<Marker> addMarker(MarkerOptions options) async {
    final MarkerOptions effectiveOptions =
        MarkerOptions.defaultOptions.copyWith(options);
    final String markerId = await _channel.invokeMethod(
      'marker#add',
      <String, dynamic>{
        'options': _Codex.markerToJson(effectiveOptions),
      },
    );
    final Marker marker = Marker(markerId, effectiveOptions);
    _markers[markerId] = marker;
    notifyListeners();
    return marker;
  }

  /// Updates the specified [marker] with the given [changes]. The marker must
  /// be a current member of the [markers] set.
  ///
  /// Change listeners are notified once the marker has been updated on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> updateMarker(Marker marker, MarkerOptions changes) async {
    assert(marker != null);
    assert(_markers[marker._id] == marker);
    assert(changes != null);
    await _channel.invokeMethod('marker#update', <String, dynamic>{
      'marker': marker._id,
      'options': _Codex.markerToJson(changes),
    });
    marker._options = marker._options.copyWith(changes);
    notifyListeners();
  }

  /// Removes the specified [marker] from the map. The marker must be a current
  /// member of the [markers] set.
  ///
  /// Change listeners are notified once the marker has been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> removeMarker(Marker marker) async {
    assert(marker != null);
    assert(_markers[marker._id] == marker);
    await _removeMarker(marker._id);
    notifyListeners();
  }

  /// Removes all [markers] from the map.
  ///
  /// Change listeners are notified once all markers have been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> clearMarkers() async {
    assert(_markers != null);
    final List<String> markerIds = List<String>.from(_markers.keys);
    for (String id in markerIds) {
      await _removeMarker(id);
    }
    notifyListeners();
  }

  /// Helper method to remove a single marker from the map. Consumed by
  /// [removeMarker] and [clearMarkers].
  ///
  /// The returned [Future] completes once the marker has been removed from
  /// [_markers].
  Future<void> _removeMarker(String id) async {
    await _channel.invokeMethod('marker#remove', <String, dynamic>{
      'marker': id,
    });
    _markers.remove(id);
  }

  /// Adds a polyline to the map, configured using the specified custom [options].
  ///
  /// Change listeners are notified once the polyline has been added on the
  /// platform side.
  ///
  /// The returned [Future] completes with the added polyline once listeners have
  /// been notified.
  Future<Polyline> addPolyline(PolylineOptions options) async {
    final PolylineOptions effectiveOptions =
        PolylineOptions.defaultOptions.copyWith(options);
    final String polylineId = await _channel.invokeMethod(
      'polyline#add',
      <String, dynamic>{
        'options': _Codex.polylineToJson(effectiveOptions),
      },
    );
    final Polyline polyline = Polyline(polylineId, effectiveOptions);
    _polylines[polylineId] = polyline;
    notifyListeners();
    return polyline;
  }

  /// Updates the specified [polyline] with the given [changes]. The polyline must
  /// be a current member of the [polylines] set.
  ///
  /// Change listeners are notified once the polyline has been updated on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> updatePolyline(
      Polyline polyline, PolylineOptions changes) async {
    assert(polyline != null);
    assert(_polylines[polyline._id] == polyline);
    assert(changes != null);
    await _channel.invokeMethod('polyline#update', <String, dynamic>{
      'polyline': polyline._id,
      'options': _Codex.polylineToJson(changes),
    });
    polyline._options = polyline._options.copyWith(changes);
    notifyListeners();
  }

  /// Removes the specified [polyline] from the map. The polyline must be a current
  /// member of the [polylines] set.
  ///
  /// Change listeners are notified once the polyline has been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> removePolyline(Polyline polyline) async {
    assert(polyline != null);
    assert(_polylines[polyline._id] == polyline);
    await _removePolyline(polyline._id);
    notifyListeners();
  }

  /// Removes all [polylines] from the map.
  ///
  /// Change listeners are notified once all polylines have been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> clearPolylines() async {
    assert(_polylines != null);
    final List<String> polylineIds = List<String>.from(_polylines.keys);
    for (String id in polylineIds) {
      await _removePolyline(id);
    }
    notifyListeners();
  }

  /// Helper method to remove a single polyline from the map. Consumed by
  /// [removePolyline] and [clearPolylines].
  ///
  /// The returned [Future] completes once the polyline has been removed from
  /// [_polylines].
  Future<void> _removePolyline(String id) async {
    await _channel.invokeMethod('polyline#remove', <String, dynamic>{
      'polyline': id,
    });
    _polylines.remove(id);
  }

  /// Adds a polygon to the map, configured using the specified custom [options].
  ///
  /// Change listeners are notified once the polygon has been added on the
  /// platform side.
  ///
  /// The returned [Future] completes with the added polygon once listeners have
  /// been notified.
  Future<Polygon> addPolygon(PolygonOptions options) async {
    final PolygonOptions effectiveOptions =
        PolygonOptions.defaultOptions.copyWith(options);
    final String polygonId = await _channel.invokeMethod(
      'polygon#add',
      <String, dynamic>{
        'options': _Codex.polygonToJson(effectiveOptions),
      },
    );
    final Polygon polygon = Polygon(polygonId, effectiveOptions);
    _polygons[polygonId] = polygon;
    notifyListeners();
    return polygon;
  }

  /// Updates the specified [polygon] with the given [changes]. The polygon must
  /// be a current member of the [polygons] set.
  ///
  /// Change listeners are notified once the polygon has been updated on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> updatePolygon(Polygon polygon, PolygonOptions changes) async {
    assert(polygon != null);
    assert(_polygons[polygon._id] == polygon);
    assert(changes != null);
    await _channel.invokeMethod('polygon#update', <String, dynamic>{
      'polygon': polygon._id,
      'options': _Codex.polygonToJson(changes),
    });
    polygon._options = polygon._options.copyWith(changes);
    notifyListeners();
  }

  /// Removes the specified [polygon] from the map. The polygon must be a current
  /// member of the [polygons] set.
  ///
  /// Change listeners are notified once the polygon has been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> removePolygon(Polygon polygon) async {
    assert(polygon != null);
    assert(_polygons[polygon._id] == polygon);
    await _removePolygon(polygon._id);
    notifyListeners();
  }

  /// Removes all [polygons] from the map.
  ///
  /// Change listeners are notified once all polygons have been removed on the
  /// platform side.
  ///
  /// The returned [Future] completes once listeners have been notified.
  Future<void> clearPolygons() async {
    assert(_polygons != null);
    final List<String> polygonsIds = List<String>.from(_polygons.keys);
    for (String id in polygonsIds) {
      await _removePolygon(id);
    }
    notifyListeners();
  }

  /// Helper method to remove a single polygon from the map. Consumed by
  /// [removePolygon] and [clearPolygons].
  ///
  /// The returned [Future] completes once the polygon has been removed from
  /// [_polygons].
  Future<void> _removePolygon(String id) async {
    await _channel.invokeMethod('polygon#remove', <String, dynamic>{
      'polygon': id,
    });
    _polygons.remove(id);
  }
}
