part of google_maps_flutter_sc;

/// A wrapper class that can be used in a PointQuadTree Created from 
/// a LatLng and optional intensity: point coordinates of the LatLng 
/// and the intensity value can be accessed from it later.
class WeightedLatLng {
  WeightedLatLng(
    this.point, {
    this.intensity = 1,
  });

  /// LatLng to add to wrapper
  final LatLng point;

  /// Intensity to use: should be greater than 0 Default value is 1.
  /// This represents the "importance" or "value" of this particular
  /// point Higher intensity values map to higher colours.
  /// Intensity is additive: having two points of intensity 1 at the same
  /// location is identical to having one of intensity 2.
  final double intensity;
}
