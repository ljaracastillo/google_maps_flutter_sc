part of google_maps_flutter_sc;

/// A circle on the earth's surface (spherical cap).
class Circle {
  Circle(this._id, this._options);

  /// A unique identifier for this circle.
  ///
  /// The identifier is an arbitrary unique string.
  final String _id;
  String get id => _id;

  CircleOptions _options;

  /// The circle configuration options most recently applied programmatically
  /// via the map controller.
  CircleOptions get options => _options;
}

/// Configuration options for [Circle] instances.
///
/// When used to change configuration, null values will be interpreted as
/// "do not change this configuration option"; except for the pattern, a null pattern will
/// set the stroke pattern to the default (solid).
class CircleOptions {
  const CircleOptions({
    this.center,
    this.radius,
    this.fillColor,
    this.strokeColor,
    this.strokePattern,
    this.strokeWidth,
    this.clickable,
    this.visible,
    this.zIndex,
  });

  /// The center of the Circle is specified as a LatLng.
  final LatLng center;

  /// The radius of the circle, specified in meters.
  /// It should be zero or greater.
  final double radius;

  /// The color of the circle fill in ARGB format, the same format used by Color.
  /// The default value is transparent (0x00000000).
  final int fillColor;

  /// The color of the circle outline in ARGB format, the same format used by Color.
  /// The default value is black (0xff000000).
  final int strokeColor;

  /// Solid (default, represented by null) or a sequence of [PatternItem]
  /// objects to be repeated along the polygon's outline. Available [PatternItem] types:
  /// * Gap (defined by gap length in pixels),
  /// * Dash (defined by stroke width and dash length in pixels)
  /// * Dot (circular, centered on the polygon's outline, diameter defined by stroke width in pixels).
  final List<PatternItem> strokePattern;

  /// The width of the circle's outline in screen pixels.
  /// The width is constant and independent of the camera's zoom level.
  /// The default value is 10.
  final double strokeWidth;

  /// If you want to handle events fired when the user clicks the polygon,
  /// set this property to true. You can change this value at any time.
  /// The default is false. If this property is set to true, your app will
  /// receive notifications to the [GoogleMapController#onPolygonTapped].
  final bool clickable;

  /// Indicates if the circle is visible or invisible, i.e., whether it is
  /// drawn on the map. An invisible circle is not drawn, but retains all
  /// of its other properties. The default is true, i.e., visible.
  final bool visible;

  /// The order in which this polygon is drawn with respect to other overlays,
  /// including Polylines, Circles, GroundOverlays and TileOverlays,
  /// but not Markers. An overlay with a larger z-index is drawn over
  /// overlays with smaller z-indices. The order of overlays with the same
  /// z-index value is arbitrary. The default is 0.
  final double zIndex;

  /// Default circle options.
  ///
  /// Specifies a circles that
  /// * does not consume tap events; [clickable] is false
  /// * is transparent; [fillColor] is 0x00000000
  /// * has no radius; [raius] is 0.0
  /// * has no center; [center] is null
  /// * is visible; [visible] is true
  /// * the stroke has a width of 10; [strokeWidth] is 10
  /// * the stroke has no pattern: [strokePattern] is null
  /// * the stroke color is black; [strokeColor] is 0xff000000
  /// * is placed at the base of the drawing order; [zIndex] is 0.0
  static const CircleOptions defaultOptions = CircleOptions(
    center: null,
    radius: 0.0,
    fillColor: 0x00000000,
    strokeColor: 0xff000000,
    strokePattern: null,
    strokeWidth: 10,
    clickable: false,
    visible: true,
    zIndex: 0.0,
  );

  /// Creates a new options object whose values are the same as this instance,
  /// unless overwritten by the specified [changes].
  ///
  /// Returns this instance, if [changes] is null.
  CircleOptions copyWith(CircleOptions changes) {
    if (changes == null) {
      return this;
    }
    return CircleOptions(
      center: changes.center ?? center,
      radius: changes.radius ?? radius,
      fillColor: changes.fillColor ?? fillColor,
      strokeColor: changes.strokeColor ?? strokeColor,
      strokePattern: changes.strokePattern,
      strokeWidth: changes.strokeWidth ?? strokeWidth,
      clickable: changes.clickable ?? clickable,
      visible: changes.visible ?? visible,
      zIndex: changes.zIndex ?? zIndex,
    );
  }
}
