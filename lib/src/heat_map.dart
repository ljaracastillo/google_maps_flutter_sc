part of google_maps_flutter_sc;

/// A heatmap on the earth's surface.
class HeatMap {
  HeatMap(this._id, this._options);

  /// A unique identifier for this circle.
  ///
  /// The identifier is an arbitrary unique string.
  final String _id;
  String get id => _id;

  HeatMapOptions _options;

  /// The circle configuration options most recently applied programmatically
  /// via the map controller.
  HeatMapOptions get options => _options;
}

/// Configuration options for [HeatMap] instances.
///
/// When used to change configuration, null values will be interpreted as
/// "do not change this configuration option"; except for the pattern, a null pattern will
/// set the stroke pattern to the default (solid).
class HeatMapOptions {
  const HeatMapOptions({
    this.data,
    this.gradient,
    this.opacity,
    this.radius,
    this.visible,
    this.weightedData,
  });

  /// Data set of points to use in the heatmap, as LatLngs.
  final List<LatLng> data;

  /// Color gradient to set
  final Gradient gradient;

  /// This is the opacity of the entire heatmap layer, and ranges from 0 to 1.
  /// The default is 0.7
  final double opacity;

  /// The size of the Gaussian blur applied to the heatmap, expressed in pixels.
  /// The default is 20. Must be between 10 and 50.
  final int radius;

  /// Indicates if the heat map is visible or invisible, i.e., whether it is drawn
  /// on the map. An invisible heat map is not drawn, but retains all
  /// of its other properties. The default is true, i.e., visible.
  final bool visible;

  /// Collection of WeightedLatLngs to put into quadtree.
  final List<WeightedLatLng> weightedData;

  /// Default circle options.
  ///
  /// Specifies a heatmap that
  /// * is visible; [visible] is true
  /// * has a radius of 20; [radius] is 20
  /// * the opacity is 0.7; [opacity] is 0.7
  static HeatMapOptions defaultOptions = HeatMapOptions(
    data: null,
    gradient: null,
    opacity: 0.7,
    radius: 20,
    visible: true,
    weightedData: null,
  );

  /// Creates a new options object whose values are the same as this instance,
  /// unless overwritten by the specified [changes].
  ///
  /// Returns this instance, if [changes] is null.
  HeatMapOptions copyWith(HeatMapOptions changes) {
    if (changes == null) {
      return this;
    }
    return HeatMapOptions(
      data: changes.data ?? data,
      gradient: changes.gradient ?? gradient,
      opacity: changes.opacity ?? opacity,
      radius: changes.radius ?? radius,
      visible: changes.visible ?? visible,
      weightedData: changes.weightedData ?? weightedData,
    );
  }
}
