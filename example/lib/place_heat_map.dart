import 'package:flutter/material.dart';
import 'package:google_maps_flutter_sc/google_maps_flutter_sc.dart';

import 'page.dart';

class PlaceHeatMapPage extends Page {
  PlaceHeatMapPage() : super(const Icon(Icons.map), 'Place heat map');

  @override
  Widget build(BuildContext context) {
    return const PlaceHeatMapBody();
  }
}

class PlaceHeatMapBody extends StatefulWidget {
  const PlaceHeatMapBody();

  @override
  State<StatefulWidget> createState() => PlaceHeatMapBodyState();
}

class PlaceHeatMapBodyState extends State<PlaceHeatMapBody> {
  PlaceHeatMapBodyState();

  GoogleMapController controller;
  int _heatMapCount = 0;
  HeatMap _selectedHeatMap;

  int radiusIndex = 0;
  List<int> radius = <int>[10, 20, 30, 40, 50];

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
  }

  void _updateSelectedHeatMap(HeatMapOptions changes) {
    controller.updateHeatMap(_selectedHeatMap, changes);
  }

  void _add() {
    List<Color> colors = [
      Color.fromARGB(255, 46, 145, 0), // green
      Color.fromARGB(255, 228, 181, 12), // yellow
      Color.fromARGB(255, 236, 113, 61), // orange
      Color.fromARGB(255, 210, 60, 58), // red
      Color.fromARGB(255, 121, 79, 146), // purple
    ];

    List<double> stops = [0.2, 0.4, 0.6, 0.8, 1];

    controller
        .addHeatMap(
      HeatMapOptions(
        weightedData: _createWeightedPoints(),
        gradient: LinearGradient(
          colors: colors,
          stops: stops,
        ),
      ),
    )
        .then((heatMap) {
      setState(() {
        _selectedHeatMap = heatMap;
      });
    });
    setState(() {
      _heatMapCount += 1;
    });
  }

  void _remove() {
    controller.removeHeatMap(_selectedHeatMap);
    setState(() {
      _selectedHeatMap = null;
      _heatMapCount -= 1;
    });
  }

  Future<void> _toggleVisible() async {
    _updateSelectedHeatMap(
      HeatMapOptions(
        visible: !_selectedHeatMap.options.visible,
      ),
    );
  }

  Future<void> _changeRadius() async {
    _updateSelectedHeatMap(
      HeatMapOptions(
        radius: radius[++radiusIndex % radius.length],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Center(
          child: SizedBox(
            width: 350.0,
            height: 300.0,
            child: GoogleMap(
              onMapCreated: _onMapCreated,
              options: GoogleMapOptions(
                cameraPosition: const CameraPosition(
                  target: LatLng(52.4478, -3.5402),
                  zoom: 7.0,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        FlatButton(
                          child: const Text('add'),
                          onPressed: (_heatMapCount == 1) ? null : _add,
                        ),
                        FlatButton(
                          child: const Text('remove'),
                          onPressed:
                              (_selectedHeatMap == null) ? null : _remove,
                        ),
                        FlatButton(
                          child: const Text('toggle visible'),
                          onPressed: (_selectedHeatMap == null)
                              ? null
                              : _toggleVisible,
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        FlatButton(
                          child: const Text('change radius'),
                          onPressed:
                              (_selectedHeatMap == null) ? null : _changeRadius,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  List<WeightedLatLng> _createWeightedPoints() {
    final List<WeightedLatLng> points = <WeightedLatLng>[];
    points.add(WeightedLatLng(_createLatLng(52.4153, -4.0829), intensity: 1.0));
    points.add(
        WeightedLatLng(_createLatLng(52.416033, -4.078426), intensity: 0.2));
    points.add(
        WeightedLatLng(_createLatLng(52.417761, -4.082020), intensity: 0.3));
    points.add(
        WeightedLatLng(_createLatLng(52.412883, -4.078864), intensity: 0.4));
    return points;
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }
}
