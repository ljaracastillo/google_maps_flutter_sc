import 'package:flutter/material.dart';
import 'package:google_maps_flutter_sc/google_maps_flutter_sc.dart';

import 'page.dart';

class PlacePolygonPage extends Page {
  PlacePolygonPage() : super(const Icon(Icons.map), 'Place polygon');

  @override
  Widget build(BuildContext context) {
    return const PlacePolygonBody();
  }
}

class PlacePolygonBody extends StatefulWidget {
  const PlacePolygonBody();

  @override
  State<StatefulWidget> createState() => PlacePolygonBodyState();
}

class PlacePolygonBodyState extends State<PlacePolygonBody> {
  PlacePolygonBodyState();

  GoogleMapController controller;
  int _polygonCount = 0;
  Polygon _selectedPolygon;

  int colorsIndex = 0;
  List<int> colors = <int>[
    0xFF000000,
    0xFF2196F3,
    0xFFF44336,
  ];

  int widthsIndex = 0;
  List<double> widths = <double>[10.0, 20.0, 5.0];

  int jointTypesIndex = 0;
  List<int> jointTypes = <int>[
    JointType.mitered,
    JointType.bevel,
    JointType.round
  ];

  int patternsIndex = 0;
  List<List<PatternItem>> patterns = <List<PatternItem>>[
    null,
    <PatternItem>[
      PatternItem.dash(30.0),
      PatternItem.gap(20.0),
      PatternItem.dot,
      PatternItem.gap(20.0)
    ],
    <PatternItem>[PatternItem.dash(30.0), PatternItem.gap(20.0)],
    <PatternItem>[PatternItem.dot, PatternItem.gap(10.0)],
  ];

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
    controller.onPolygonTapped.add(_onPolygonTapped);
  }

  @override
  void dispose() {
    controller?.onPolygonTapped?.remove(_onPolygonTapped);
    super.dispose();
  }

  void _onPolygonTapped(Polygon polygon) {
    setState(() {
      _selectedPolygon = polygon;
    });
  }

  void _updateSelectedPolygon(PolygonOptions changes) {
    controller.updatePolygon(_selectedPolygon, changes);
  }

  void _add() {
    controller.addPolygon(PolygonOptions(
      clickable: true,
      points: _createPoints(),
    ));
    setState(() {
      _polygonCount += 1;
    });
  }

  void _remove() {
    controller.removePolygon(_selectedPolygon);
    setState(() {
      _selectedPolygon = null;
      _polygonCount -= 1;
    });
  }

  Future<void> _toggleGeodesic() async {
    _updateSelectedPolygon(
      PolygonOptions(
          geodesic: !_selectedPolygon.options.geodesic,
          strokePattern: _selectedPolygon.options.strokePattern),
    );
  }

  Future<void> _toggleVisible() async {
    _updateSelectedPolygon(
      PolygonOptions(
          visible: !_selectedPolygon.options.visible,
          strokePattern: _selectedPolygon.options.strokePattern),
    );
  }

  Future<void> _changeColor() async {
    _updateSelectedPolygon(
      PolygonOptions(
          fillColor: colors[++colorsIndex % colors.length],
          strokePattern: _selectedPolygon.options.strokePattern),
    );
  }

  Future<void> _changeWidth() async {
    _updateSelectedPolygon(
      PolygonOptions(
          strokeWidth: widths[++widthsIndex % widths.length],
          strokePattern: _selectedPolygon.options.strokePattern),
    );
  }

  Future<void> _changeJointType() async {
    _updateSelectedPolygon(
      PolygonOptions(
          strokeJointType: jointTypes[++jointTypesIndex % jointTypes.length],
          strokePattern: _selectedPolygon.options.strokePattern),
    );
  }

  Future<void> _changePattern() async {
    _updateSelectedPolygon(
      PolygonOptions(strokePattern: patterns[++patternsIndex % patterns.length]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Center(
          child: SizedBox(
            width: 350.0,
            height: 300.0,
            child: GoogleMap(
              onMapCreated: _onMapCreated,
              options: GoogleMapOptions(
                cameraPosition: const CameraPosition(
                  target: LatLng(52.4478, -3.5402),
                  zoom: 7.0,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        FlatButton(
                          child: const Text('add'),
                          onPressed: (_polygonCount == 1) ? null : _add,
                        ),
                        FlatButton(
                          child: const Text('remove'),
                          onPressed:
                              (_selectedPolygon == null) ? null : _remove,
                        ),
                        FlatButton(
                          child: const Text('toggle visible'),
                          onPressed: (_selectedPolygon == null)
                              ? null
                              : _toggleVisible,
                        ),
                        FlatButton(
                          child: const Text('toggle geodesic'),
                          onPressed: (_selectedPolygon == null)
                              ? null
                              : _toggleGeodesic,
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        FlatButton(
                          child: const Text('change width'),
                          onPressed:
                              (_selectedPolygon == null) ? null : _changeWidth,
                        ),
                        FlatButton(
                          child: const Text('change joint type'),
                          onPressed: (_selectedPolygon == null)
                              ? null
                              : _changeJointType,
                        ),
                        FlatButton(
                          child: const Text('change color'),
                          onPressed:
                              (_selectedPolygon == null) ? null : _changeColor,
                        ),
                        FlatButton(
                          child: const Text('change pattern'),
                          onPressed: (_selectedPolygon == null)
                              ? null
                              : _changePattern,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  List<LatLng> _createPoints() {
    final List<LatLng> points = <LatLng>[];
    points.add(_createLatLng(51.4816, -3.1791));
    points.add(_createLatLng(53.0430, -2.9925));
    points.add(_createLatLng(53.1396, -4.2739));
    points.add(_createLatLng(52.4153, -4.0829));
    return points;
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }
}
